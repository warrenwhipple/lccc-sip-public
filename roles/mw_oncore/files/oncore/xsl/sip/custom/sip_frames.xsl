<?xml version="1.0" encoding="iso-8859-1"?>  
 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
<xsl:output method="html" indent="yes" version="4.0"/>

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="sip_frames">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<title>Study Information Portal</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>

<frameset rows="102,*" frameborder="0" framespacing="0" border="0" >
  <frame src="/sip/SIPControlServlet?hdn_function=SIP_CC" name="top" noresize="noresize" marginheight="0" />
  
  <frameset cols="300,*" frameborder="0" framespacing="0" border="0" >
    <frame src="/sip/SIPControlServlet?hdn_function=SIP_SEARCH" name="search" />
    <frame src="/sip/SIPControlServlet?hdn_function=SIP_INSTRUCTIONS" name="content" scrolling="yes" />
    <noframes>
    </noframes>
  </frameset>
</frameset>
</html>

</xsl:template>

</xsl:stylesheet>