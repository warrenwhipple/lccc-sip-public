<?xml version="1.0" encoding="iso-8859-1"?>  
 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
<xsl:output method="html" indent="yes" version="4.0"/>

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="sip_cc">
<html lang="en">

<head>
<title>Cancer Center</title>

<script type="text/javascript" src="/sip/scripts/sipUtil.js"></script>
<script type="text/javascript" src="/sip/scripts/sipStyle.js"></script>

<link rel="stylesheet" type="text/css" media="print" href="styles/sip_print.css" />

</head>
<body>

<div id="noprint">
<table width="100%">
 <tr>
   <td align="center">
    <a href="http://www.percipenz.com/index.html"  target="_top"><img width="78" height="78" src="/sip/images/logo_demo.gif" border="0" alt="logo"></img></a>
   </td>
 </tr>
</table>
</div>

</body>
</html>

</xsl:template>

</xsl:stylesheet>
