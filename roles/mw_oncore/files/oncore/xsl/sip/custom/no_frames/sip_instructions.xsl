<?xml version="1.0" encoding="iso-8859-1"?>  
 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    version="1.0">
<xsl:output method="html" indent="yes" version="4.0"/>

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>
  
<xsl:template match="sip_instructions">
<html lang="en">
 
<head>

    <title>Sip Instructions</title>
	<script type="text/javascript" src="/sip/scripts/sipUtil.js"></script>
	<script type="text/javascript" src="/sip/scripts/sipStyle.js"></script>
</head>
<body class="welcome-body3">
<table border="0" cellpadding="2" cellspacing="1" align="left" class="table-color" width="650">	
	<tr class="tbar-color">
        <td class="form-heading" align="center">Search Instructions</td>
    </tr>
    <!--
    <tr>
    	<td width="68" align="center"><img src="/sip/images/logo_demo.gif" border="0"></img><br/></td>
    </tr>
    -->
    <tr> 
		<td class="table-color-instr" align="left">
			To search for a clinical trial (protocol), use any or all of the search fields to the left:
			<ul>
			<li> Type search criteria in any of the text fields. Skip any items that are unknown or not applicable.</li>
			<li>Use the dropdown menus if you wish to limit your search by Age Group</li>
			<li>To limit the search to Phase I, Cancer Prevention or Cancer Control, check the appropriate boxes  </li>
			<li>Browse by Disease Site, Drug, Principal Investigator (PI) or Study Site, use the magnification icon to the right of the field <img border="0" src="/sip/images/browse.gif" align="top"></img></li>
			</ul>
			<p>For all searches and for all browse results, select the Search button at the bottom after you have refined your  criteria. </p>
			<p><em><strong>Notes:</strong></em></p>
			<blockquote>
			<p><strong>Phase I</strong> clinical trials are typically not disease specific and can be searched separately by clicking on the Phase I checkbox. (If you do not select Phase I, trials in any phase that match your other search terms will be retrieved.)</p>
			<p><strong>Cancer Prevention</strong> clinical trials test whether taking certain medicines, vitamins, minerals, or food supplements may lower the risk of a certain type of cancer.</p>
			<p><strong>Cancer Control</strong> clinical trials test whether certain drugs reduce side effects from chemotherapy and other primary treatments.</p>
			</blockquote>
			<p>For further information on our clinical trials, please call the Demo Cancer Center cancer information line at 1-800-xxx-xxxx</p>
			<p>&#160;</p>
		</td>
    </tr>
</table>
</body>
</html>

</xsl:template>

</xsl:stylesheet>
