<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" doctype-system="http://www.w3.org/TR/html4/loose.dtd" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" indent="yes"/>

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="sip_search">
<xsl:param name="oncorehost">https://oncore.unc.edu/sip/</xsl:param>
<xsl:param name="wwwhost">https://unclineberger.org/</xsl:param>
<html lang="en">

<head>
<title>Study Information Portal</title>
<!-- <base href="https://oncore.unc.edu" />
 -->
<script type="text/javascript" src="{$oncorehost}scripts/sipUtil.js"></script>
<!-- <script type="text/javascript" src="{$oncorehost}scripts/sipStyle.js"></script> -->
<script type="text/javascript" src="{$oncorehost}scripts/dojo-0.3.1-ajax/dojo.js"></script>
<script type="text/javascript">
	function displayDiseaseResults(category,categoryDescription,paramName,hdnParamName)
	{
			var paramValue = eval('document.frm_sip_search.'+paramName+'.value');

			var url = '<xsl:value-of select="$oncorehost" />SIPBrowseServlet?hdn_function=DISEASE_BROWSE&amp;param_name='+ paramName + '&amp;param_value=' + paramValue + '&amp;hdn_param_name=' + hdnParamName + '&amp;category=' + category + '&amp;category_description=' + categoryDescription;
			MM_openBrWindow(url,'BrowseResults','scrollbars=yes,resizable=no,dependent=yes,left=450,top=180,width=500,height=400');

			return true;
	}

	function displayDomainResults(category,categoryDescription,paramName)
	{
			var paramValue = eval('document.frm_sip_search.'+paramName+'.value');

			var url = '<xsl:value-of select="$oncorehost" />SIPBrowseServlet?hdn_function=DOMAIN_BROWSE&amp;param_name='+ paramName + '&amp;param_value=' + paramValue + '&amp;category=' + category + '&amp;category_description=' + categoryDescription;
			MM_openBrWindow(url,'BrowseResults','scrollbars=yes,resizable=no,dependent=yes,left=450,top=180,width=500,height=400');

			return true;
	}

	function displayStaffResults(staffRole,staffRoleDescription,paramName)
	{
			var pi = document.getElementById("pi").innerHTML;
			var paramValue = eval('document.frm_sip_search.'+paramName+'.value');

			var url = '<xsl:value-of select="$oncorehost" />SIPBrowseServlet?hdn_function=STAFF_BROWSE&amp;param_name='+ paramName + '&amp;param_value=' + paramValue + '&amp;staff_role=' + pi + '&amp;staff_role_description=' + staffRoleDescription;
			MM_openBrWindow(url,'BrowseResults','scrollbars=yes,resizable=yes,dependent=yes,left=450,top=180,width=500,height=400');

			return true;
	}

	function setParamValue(paramName,paramValue)
	{
			eval('document.frm_sip_search.'+paramName+'.value="'+paramValue+'"');
	}

	function doNothing(){}

	function validate()
	{
		var msg = "";
		if(eval(document.frm_sip_search.exclude_flag))
		{
			var excludeFlag = document.frm_sip_search.exclude_flag.checked;
			var therapy = document.frm_sip_search.therapy.value;

			if (excludeFlag &amp;&amp; isEmpty(therapy))
					msg = "Please enter therapy to be excluded."
			if (msg.length &gt; 0)
			{
				alert(msg);
				return false;
			}
		}
		setSearchParamTxt();
		return true;
	}

	function setSearchParamTxt()
	{
		document.frm_sip_search.disease_site_txt.value=document.getElementById("ds").innerHTML;
		document.frm_sip_search.keyword_txt.value=document.getElementById("key").innerHTML;
		document.frm_sip_search.drug_txt.value=document.getElementById("drg").innerHTML;
		document.frm_sip_search.therapy_txt.value=document.getElementById("ther").innerHTML;
		document.frm_sip_search.phase_txt.value=document.getElementById("phs").innerHTML;
		document.frm_sip_search.cancer_prevention_txt.value=document.getElementById("cp").innerHTML;
		document.frm_sip_search.cancer_control_txt.value=document.getElementById("cc").innerHTML;
		document.frm_sip_search.protocol_no_txt.value=document.getElementById("pcl").innerHTML;
		document.frm_sip_search.pi_txt.value=document.getElementById("pi").innerHTML;
		document.frm_sip_search.study_site_txt.value=document.getElementById("ss").innerHTML;
	}
    function displayProtocolSummary(protocolId, protocolNo)
	{
		var url="<xsl:value-of select="$oncorehost" />SIPMain?hdn_function=SIP_PROTOCOL_SUMMARY&amp;protocol_id=" + protocolId + "&amp;protocol_no=" + protocolNo;
		dojo.io.bind({
			url: url,
			method: "GET",
			mimetype: "text/plain",
			load: function(type, data, event) {
				var divNode = document.getElementById("mainContent");
				divNode.innerHTML = data;
				changeTitle('Protocol Summary');
			}
		});
	}
	function displayAttachment(fileName, attachmentId)
	{
		var url = '<xsl:value-of select="$oncorehost" />SIPMain?hdn_function=PCL_ATTACHMENTS&amp;hdn_function_type=INQUIRY';
		url = url + '&amp;attachment_id=' + attachmentId + '&amp;protocol_id=&amp;file_name=' + fileName;
		MM_openBrWindow(url,'Attachments','scrollbars=yes,resizable=yes');
	}
	function searchSubmit() {
		document.frm_sip_search.hdn_function.value = "SIP_PROTOCOL_LISTINGS";
		dojo.io.bind({
			url: "<xsl:value-of select="$oncorehost" />SIPMain",
			method: "GET",
			formNode: document.frm_sip_search,
			mimetype: "text/plain",
			load: function(type, data, event) {
				var divNode = document.getElementById("mainContent");
				divNode.innerHTML = data;
				changeTitle('Search Results');
			}
		});
	}
	function changeTitle(title) {
		document.getElementById('appTitle').innerHTML = title;
	}
</script>
<!-- <link rel="stylesheet" type="text/css" media="print" href="styles/sip_print.css" />
 -->
<meta content="Clinical trials are extremely important to cancer patients, who may have exhausted all other forms of treatment, yet still have active disease." name="DC.description" />
<meta content="Clinical trials are extremely important to cancer patients, who may have exhausted all other forms of treatment, yet still have active disease." name="description" />
<meta content="text/html" name="DC.format" />
<meta content="Page" name="DC.type" />
<meta content="en" name="DC.language" />
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<base href="https://unclineberger.org/" />
<!-- Stylesheet blob scrapped from site in development mode -->
<link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/reset.css" /><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/member.css" /><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/collective.js.jqueryui.custom.min.css" /><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/authoring.css" /><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/++resource++tinymce.stylesheets/tinymce.css" /><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/++resource++plone.app.jquerytools.dateinput.css" /><style type="text/css" media="all">@import url(/portal_css/uncsom.SOMTheme/++resource++ContentWellPortlets.styles/ContentWellPortlets.css);</style><style type="text/css" media="screen">@import url(/portal_css/uncsom.SOMTheme/++resource++easyslider-portlet.css);</style><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/++resource++plone.formwidget.datetime/styles.css" /><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/++resource++plone.app.event/event.css" /><style type="text/css" media="screen">@import url(/portal_css/uncsom.SOMTheme/++resource++plone.formwidget.autocomplete/jquery.autocomplete.css);</style><style type="text/css" media="screen">@import url(/portal_css/uncsom.SOMTheme/++resource++plone.formwidget.contenttree/contenttree.css);</style><style type="text/css" media="screen">@import url(/portal_css/uncsom.SOMTheme/carousel.css);</style><link rel="stylesheet" type="text/css" media="all" href="/portal_css/uncsom.SOMTheme/++resource++collective.js.fullcalendar/fullcalendar.css" /><style type="text/css" media="all">@import url(/portal_css/uncsom.SOMTheme/solgema_contextualcontentmenu.css);</style><style type="text/css" media="screen">@import url(/portal_css/uncsom.SOMTheme/++resource++collective.js.colorpicker.css);</style><style type="text/css" media="all">@import url(/portal_css/uncsom.SOMTheme/som_gss.css);</style><link rel="stylesheet" type="text/css" href="/portal_css/uncsom.SOMTheme/++theme++SOMTheme/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/portal_css/uncsom.SOMTheme/++theme++SOMTheme/css/bootstrap-responsive.min.css" /><link rel="stylesheet" type="text/css" href="/portal_css/uncsom.SOMTheme/++theme++SOMTheme/css/bootstrap-custom.css" /><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/++theme++SOMTheme/css/ploneui.css" /><link rel="stylesheet" type="text/css" href="/portal_css/uncsom.SOMTheme/++theme++SOMTheme/css/theme.css" /><link rel="stylesheet" type="text/css" media="all" href="/portal_css/uncsom.SOMTheme/++resource++plone.formwidget.recurrence/jquery.recurrenceinput.css" /><link rel="stylesheet" type="text/css" media="all" href="/portal_css/uncsom.SOMTheme/++resource++plone.formwidget.recurrence/integration.css" /><link rel="stylesheet" type="text/css" href="/portal_css/uncsom.SOMTheme/ploneCustom.css" />
<style>
#portal-header {
    background: url("/++theme++SOMTheme/img/shadow.png") repeat-x scroll left bottom, url("/lccc-patientcare-banner") no-repeat scroll left 30px lightgray;
}
</style>

<!-- Scrape of js in development mode -->
<script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.jquery.js"></script>

<!-- <script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/jquery-integration.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.jquerytools.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.jquerytools.dateinput.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.jquerytools.overlayhelpers.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.jquerytools.form.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/register_function.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/plone_javascript_variables.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/nodeutilities.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/cookie_functions.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/livesearch.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++search.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/select_all.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/dragdropreorder.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/collapsiblesections.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/form_tabbing.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/popupforms.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/jquery.highlightsearchterms.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/first_input_focus.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/accessibility.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/styleswitcher.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/toc.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/collapsibleformfields.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.discussion.javascripts/comments.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/dropdown.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/table_sorter.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/calendar_formfield.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/formUnload.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/formsubmithelpers.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/unlockOnFormUnload.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/tiny_mce.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/jquery.tinymce.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/tiny_mce_gzip.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/tiny_mce_init.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/kss-bbb.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/inline_validation.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++easyslider-portlet.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.event/event.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.event.portlet_calendar.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.formwidget.autocomplete/jquery.autocomplete.min.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.formwidget.autocomplete/formwidget-autocomplete.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.formwidget.contenttree/contenttree.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/jquery.tools.min.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/carousel.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++dropdown-menu.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++collective.js.fullcalendar/fullcalendar.min.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++collective.js.fullcalendar/fullcalendar.gcal.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/solgema_contextualcontentmenu.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++collective.js.colorpicker.js/eye.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++collective.js.colorpicker.js/utils.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++collective.js.colorpicker.js/colorpicker.js"></script> -->
<script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/collective.js.jqueryui.custom.min.js"></script>
<script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++theme++SOMTheme/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++theme++SOMTheme/js/theme.js"></script>
<script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.formwidget.recurrence/jquery.tmpl-beta1.js"></script>
<script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.formwidget.recurrence/jquery.recurrenceinput.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $("input#autocomplete-type").autocomplete({
			source: ["Anus","Any Site","Bladder","Bones and Joints","Brain and Nervous System","Breast - Female","Breast - Male","Cervix Uteri","Colon","Corpus Uteri","Esophagus","Eye and Orbit","Geriatric","Hodgkin's Lymphoma","Ill-Defined Sites","Kaposi's sarcoma","Kidney","Larynx","Leukemia, not otherwise specified","Leukemia, other","Lip, Oral Cavity and Pharnyx", "Liver","Lung","Lymphoid Leukemia","Melanoma, skin","Multiple Myeloma","Myeloid and Monocytic Leukemia","Non-Hodgkin's Lymphoma","Other","Other Digestive Organ","Other Female Genital", "Other Hematopoietic","Other Male Genital","Other Skin","Other Urinary","Ovary","Pancreas","Prostate","Rectum","Small Intestine","Soft Tissue","Stomach","Thyroid","Unknown Sites"]
    });
    $("input#autocomplete-drug").autocomplete({
			source: ['20S PROTEASOME','2B3-101','5-FLUOROURACIL (5-FU)','6-MERCAPTOPURINE','6-MP','6-TG','ABI-007','ABIRATERONE','ABRAXANE','ABT-888','ABX-EGF','ACY-1215','ADRIAMYCIN','AFLIBERCEPT','ALIMTA','AMETHOPTERIN','AMN-107','AR00142866','ARA-C','ARRY142866','ARZERRA','AVASTIN','AZD6244','BAY 43-9006','BCNU','BENDAMUSTINE','BEVACIZUMAB','BKM120','BLENOXANE','BLEOMYCIN','BORTEZOMIB','BRENTUXIMAB VEDOTIN','BUSULFAN','BUSULFEX','C225','CAMPTOSAR','CAPECITABINE','CARBOPLATIN','CARMUSTINE','CASODEX','CC-5013','CDDP','CEDIRANIB','CELEBREX','CELECOXIB','CERTICAN','CERUBIDINE','CETUXIMAB','CISPLATIN','COBIMETINIB','CPX-351','CRLX101','CTX','CURCUMIN','CYCLOPHOSPHAMIDE','CYTARABINE','CYTOSAR-U','CYTOSINE ARABINOSIDE','CYTOXAN','DABRAFENIB','DACARBAZINE','DACOGEN','DARATUMUMAB','DAUNOMYCIN','DAUNORUBICIN','DCVax-Brain','DECADRON','DECITABINE','DEGARELIX','DELTASONE','DEXAMETHASONE','DNR','DOCETAXEL','DOXORUBICIN','DTIC-DOME','ELOXATIN','ERBITUX','ERLOTINIB','ETOPOSIDE','EULEXIN','EVEROLIMUS','FEMARA','FLUDARA','FLUDARABINE','FLUOROURACIL','FOLEX','FOLEXAMETHOPTERIN','FOLFIRI','FOLFOX','FOLINIC', 'G-CSF','GANETESPIB','GEMCITABINE','GEMZAR','GLIADEL WAFER','GM-CSF','GSK2141795','GW572016','HERCEPTIN','IBRUTINIB','IDELALISIB','IFEX','IFOSFAMIDE','IMATINIB','IMC-C225','INOTUZUMAB OZOGAMICIN','INTERFERON ALFA-2B','INTRON-A','IPILIMUMAB','IRINOTECAN','IXAZOMIB','LACOSAMIDE','LAPATINIB','LCV','LENALIDOMIDE','LETROZOLE','LEUCOVORIN','LEUCOVORIN CALCIUM','LJM716','MDX010','MEDI4736','MEDROL','MELFLUFEN','MERCAPTOPURINE','METFORMIN','METHOTREXATE','METHYLPREDNISONE','mFOLFOX-6','MIDOSTAURIN','MITOMYCIN','MitoXANTRONE','MK-8353','MLN4924','MORAb-004','MPDL3280A','MSB0010718C','MTX','MYLERAN','NAB PACLITAXEL','NAVELBINE','NEOSAR','NEUPOGEN','NEXAVAR','NILOTINIB','OFATUMUMAB','ONCASPAR','ONCOVEX','ONCOVIN','OPROZOMIB','OSI-774','OXALIPLATIN','PACLITAXEL','PACLITAXEL PROTEIN-BOUND','PANITUMUMAB','PARAPLATIN','PAZOPANIB','PEG-ASPARAGINASE','PEMETREXED','PERFLUTREN','PERTUZUMAB','PF-02341066','PF-03084014','PKC412','placebo','PLATINOL','POMALIDOMIDE','PREDNISONE','PRLX 93936','PS-341','PURINETHOL','RAD-001','RADIATION THERAPY','recMAGE-A3+AS15','REVLAMID','RITUXAN','RITUXIMAB','RUBEX','SARGRAMOSTIM','SORAFENIB','SU011248','SUBEROYLANILIDE HYDROXAMIC ACID','SUNITINIB','SUTENT','TALIMOGENE LAHERPAREPVEC','TARCEVA','TASIGNA','TAXOL','TAXOTERE','THIOGUANINE','TRAMETINIB','TRASTUZUMAB','TYKERB','VCR','VECTIBIX','VELBAN','VELCADE','VEPESID','VINBLASTINE','VINCRISTINE','VINORELBINE','VORINOSTAT','VP-16','VTX-2337/placebo','WELLCOVORIN','XELODA']
    });
    $("input#autocomplete-pi").autocomplete({
			source: ["Allicock, Marlyn","Anders, Carey","Asher, Gary","Atluri, Prashanti","Austin, Grace","Bae-Jump, Victoria","Basch, Ethan","Battaglini, Claudio","Bernard, Stephen","Callahan, Leigh","Carey, Lisa","Chen, Ronald","Chera, Bhisham","Coghill, James","Collichio, Frances","Comello, Maria Leonara","Cykert, Samuel","Dees, E. Claire","Doll, Kemi","Earp, Shelton","Eng, Eugenia","Evans, James","Ewend, Matthew","Fielding, Julia","Foster, Matthew","Fredrickson, Barbara","Garlock, Marie","Gershon, Timothy","Gold, Stuart","Gopal, Satish","Grilley-Olson, Juneko","Groesbeck, Parham","Hackney, Anthony","Hamilton, Jill","Hartman, Heidi","Hayes, David Neil","Hingtgen, Shawn","Hsu, Johann","Jackson, Amanda","Jamieson, Katarzyna","Jones, Ellen","Kasow, Kimberly","Kim, Jayeon","Leak, Ashley","Lee, Carrie","Lee, Yueh","Lohr, Jacob","Mauro, Matthew","Mayer, Deborah","McRee, Autumn","Merrit, Brad","Mersereau, Jennifer","Meyers, Michael","Milowsky, Matthew","Moschos, Stergios","Muss, Hyman","O'Neil, Bert","Ollila, David","Park, Leeza","Park, Steven","Pergolotti, Mackenzi","Priola, Ginna","Pruthi, Raj","Rathmell, Kimryn","Reeder-Hayes, Katie","Reeve, Bryce","Reuland, Dan","Richards, Kristy","Rini, Christine","Rivera, Patricia","Rosenstein, Donald","Sanoff, Hanna Kelly","Santacroce, Sheila","Shaheen, Nicholas","Shea, Christopher","Shea, Thomas","Smith, Angela","Song, Lixin","Stinchcombe, Thomas","Stitzenberg, Karyn","Tepper, Joel","Valle, Carmina","Van Le, Linda","Vandeventer, Hendrik","Varia, Mahesh","Voorhees, Peter","Walker, Paul","Wallen, Eric","Wang, Andy","Weiss, Jared","Whang, Young","Williams, Grant","Williams, Rebecca","Wood Jr, William A","Woods, Michael","Wu, Jing","Zagar, Timothy"]
    });
    $("input#autocomplete-study_site").autocomplete({
			source: ["Adelaide and Meath Hospital","Alamance Hem/Onc Associates","Arnold Palmer at Mountain View","Arnold Palmer at Mt. Pleasant","Arnold Palmer at Oakbrook","Aurora","Beaumont Hospital","Bellevue, Building C &amp; D","Blumenthal Cancer Center","Boice - Willis Clinic","Bon Secours","Boulder","CBC/Baptist Hosp. East","CMC - Northeast","Cancer Center of North Carolina","Cancer Center of Western North Carolina","Cancer Institute Of New Jersey","Cape Fear Valley M C/CCMI","Carolina Healthcare System","Carolinas Cancer Care -Charlotte","Carolinas Cancer Care-Cornelius","Carolinas Hematology Oncology Associates - Ballantyne","Carolinas Hematology Oncology Associates - Main","Carolinas Hematology Oncology Associates - University","Carteret General Hospital","Centennial","City of Hope CCC","Clinton Cancer Care","Colorado Springs","Colorado Springs-Nevada Avenue","Comprehensive Cancer Center at Pardee","Cork University Hospital","Costal Carolina Radiation Oncology","Dana Farber Cancer Institute","Dare County","Dartmouth-Hitchcock Medical Center","Denver- Midtown","Denver-Rose","Duke University Medical Center","East Carolina University School Of Medicine","Eastern North Carolina Medical Group","Ellis Fischel Cancer Center","Emory - Winship Cancer Institute","First Health/Moore Regional Hospital","Fox Chase Cancer Center","George Batte Cancer Center CMC-NE","Georgia Cancer Specialists","Georgia Cancer Specialists-Annex","Georgia Cancer Specialists-Athens","Georgia Cancer Specialists-Kennestone","Georgia Cancer Specialists-Macon","Georgia Cancer Specialists-Northside","Georgia Cancer Specialists-Stemmer","Goldsboro Cancer Care","High Point Regional Hospital","Highlands Oncology Group","Hope Women's Cancer Center - Asheville","IUH-Central Indiana Cancer Center","IUSimonCancer Center","Indiana University Hospital","Jacksonville Cancer Care","James Graham Brown Cancer Center","Jersey Shore Medical Center(Meridian)","Johns Hopkins University","Kinston Medical/Lenoir Memorial Hospital","LCI-Pineville (Mecklenburg Medical Group - Pineville)","Lakewood","Leo W. Jenkins Cancer Center","Littleton","Long Island Jewish Medical Center","Longmont","MD Anderson","Marion Shepard Cancer Center","Martha Jefferson Hospital","Mater Misericordiae University Hospital","Mater Private Hospital","Mayo Clinic, Rochester","Mcguire Veterans Administration","Mecklenburg Medical Group - South Park","Mecklenburg Medical Group Oncology-CCA","Mecklenburg Medical Group, MMP","MedStar Georgetown University Hospital","Medical University of South Carolina","Meta &amp; Brunskill Hematology/Oncology","Mission Cancer Center","Moffitt Cancer Center","Morristown","Moses Cone Regional Cancer Center","Mount Sinai Medical Center-Miami","Multicare Regional Cancer Center- Tacoma, WA","Multicare Regional Cancer Center-Auburn","Multicare Regional Cancer Center-Gig Harbor","Multicare Regional Cancer Center-Puyallup","Nash Cancer Care","Nash General Hospital","Nash Health Care","New Bern Cancer Center","New Hanover Regional Medical Center","New York University Langone Medical Center- The Cancer","NorthShore University Health System","Northside Hospital","Ohio State University","Oregon Health and Science University","Parker","Penrose Cancer Center","Portsmouth Naval Medical Center","Princess Margaret Hospital","Pueblo","Randolph Cancer Center","Reproductive Research Unit - UPENN","Rex Cancer Center","Rex Cancer Center of Wakefield","Rex Healthcare","Rocky Mount Cancer Care","STO Taussig Cancer Center","Savannah Hematology Oncology/St. Joseph's","Seby B. Jones Cancer Center","Sidney and Lois Eskenazi Hospital","Sky Ridge","Southeastern Medical Oncology Center","Southern Pines Womens Health Center","Springmill Clinic","St. Francis Hospital and Health Centers","St. James Hospital","St. Vincents University Hospital","Thornton","UF Health Cancer Center at Orlando","UMPC CC Beaver","UMPC CC Jefferson","UNC-CH","UPMC CC Indiana","UPMC CC McKeesport","UPMC CC Mercy","UPMC CC Monroeville (East)","UPMC CC Murtha","UPMC CC New Castle","UPMC CC Northwest","UPMC CC Passavant (HOA)","UPMC CC Passavant (OHA)","UPMC CC Sewickley (Moon)","UPMC CC St. Clair","UPMC CC St. Margaret","UPMC CC Steubenville","UPMC CC Uniontown","UPMC CC Washington","UPMC CC Windber","University College Hospital Galway","University Of Alabama At Birmingham","University Of California At San Francisco","University Of Chicago Medical Center","University Of Virginia","University of Cincinnati","University of Colorado Cancer Center","University of Pittsburgh Medical Center","University of Washington","Vanderbilt","Virginia Commonwealth University/MCV","Wake Forest University","Waterford Regional Hospital","Wilson Cancer Care","Yale University Medical Center"]
    });

});
</script>


<link rel="canonical" href="/patientcare/clinical-trials" />
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<link rel="apple-touch-icon" href="/touch_icon.png" />



<link rel="search" href="/@@search" title="Search this site" /><meta name="generator" content="Plone - http://plone.org" />
  <title>Clinical Trials - UNC Lineberger Comprehensive Cancer Center</title><title>Clinical Trials — UNC Lineberger Comprehensive Cancer Center</title>
  <!-- Typekit web fonts --><!--[if gt IE 8]><!-->
  <script type="text/javascript" src="//use.typekit.net/kak8yao.js"></script>
  <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
  <!--<![endif]-->
  <!-- Put the following javascript before the closing </head> tag. -->
<script>
  (function() {
    var cx = '017059784719810698204:lj2ibo0i4wo';
    var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s);
  })();
</script>
</head>


<body class="welcome-body1 template-document_view portaltype-document site-lccc section-patientcare subsection-clinical-trials subsection-clinical-trials-clinical-trials userrole-anonymous" id="" dir="ltr">
        <header id="portal-header" role="banner" aria-label="Site Header">
        <section id="top" aria-label="Common Links &amp; Search" class="navbar navbar-static-top container-fluid">
            <a class="btn btn-navbar" data-target=".ribbon-group" data-toggle="collapse" aria-labelledby="top">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
      </a>
            <a class="brand" href="http://www.med.unc.edu" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'med.unc.edu',,true]);">UNC School of Medicine</a>
      <div class="nav-collapse collapse ribbon-group pull-right">
                <ul class="link-group">
                    <li><a href="http://unc.edu/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'unc.edu',,true]);">UNC Chapel Hill</a></li>
                    <li><a href="https://www.unchealthcare.org/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'unchealthcare.org',,true]);">UNC Health Care</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'popular links',,true]);">Popular Links</a>
                        <ul class="dropdown-menu">
                            <li><a href="http://dir.unc.edu/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'dir.unc.edu',,true]);">UNC Directory</a></li>
                            <li><a href="http://www.med.unc.edu/maps" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'med.unc.edu/maps',,true]);">Maps &amp; Directions</a></li>
                            <li><a href="http://hsl.lib.unc.edu/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'hsl.lib.unc.edu',,true]);">Health Sciences Library</a></li>
                            <li><a href="https://outlook.unc.edu/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'outlook.unc.edu',,true]);">Webmail</a></li>
                            <li><a href="http://help.med.unc.edu/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'help.med.unc.edu',,true]);">IT Support</a></li>
                            <li><a href="http://www.med.unc.edu/gift" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'med.unc.edu/gift',,true]);">Make a Gift</a></li>
                            <li><a href="http://findadoc.unchealthcare.org/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'findadoc.unchealthcare.org',,true]);">Find a Doctor</a></li>
                            <li><a href="http://www.med.unc.edu/www/events" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'med.unc.edu/www/events',,true]);">Calendar</a></li>
                            <li><a href="http://www.med.unc.edu/careers" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'med.unc.edu/careers',,true]);">Careers</a></li>
                        </ul>
                    </li>
                </ul>
                <div class="gcse-searchbox-only" data-resultsUrl="google_search_results"></div>
                <span id="account-info">
  <li id="anon-personalbar">

        <a href="/login" id="personaltools-login">Log in</a>

  </li>
</span>
      </div>
    </section>
    <!-- H1s for both site name and page title OK within new HTML sectioning elements -->
    <section id="site_identity" aria-label="Site Identity" class="item-fluid">
        <div id="site-logo">
            <a id="site-logo-link" title="" href="">
                <img src="lccc-logo" alt="" />
            </a>
        </div>
    </section>

<nav id="horz-nav" role="navigation" aria-label="Horizontal Navigation" class="container-fluid ">
      <a class="btn btn-navigation" data-target=".nav-group" data-toggle="collapse">Navigation</a>
        <ul id="portal-globalnav" class="nav-collapse collapse nav-group navTreeLevel0">
        <li id="portaltab-about" class="plain"><a href="/about" class="plain" title="About the UNC Lineberger Comprehensive Cancer Center and the N.C. Cancer Hospital at the University of North Carolina at Chapel Hill, including map &amp; directions and contact information.">About Us</a>
        </li><li id="portaltab-patientcare" class="selected"><a href="/patientcare" class="plain" title="">Patient Care</a></li>
        <li id="portaltab-research" class="plain"><a href="/research" class="plain" title="">Research</a></li>
        <li id="portaltab-training" class="plain"><a href="/training" class="plain" title="">Training</a></li>
        <li id="portaltab-waystohelp" class="plain"><a href="/waystohelp" class="plain" title="">Ways to Help</a></li>
        <li id="portaltab-contact" class="plain"><a href="/contact" class="plain" title="">Contact</a></li>
        </ul>
</nav>

</header><div id="portlets-header" aria-label="Header Portlets" class="container-fluid">
    </div><nav id="breadcrumbs" aria-label="Breadcrumbs" class="item-fluid">
    <span id="breadcrumbs-home">
        <a class="icon-home" href="">Home</a>
        <span class="breadcrumbSeparator">
            >
        </span>
    </span>
    <span id="breadcrumbs-1" dir="ltr">

            <a href="/patientcare">Patient Care</a>
            <span class="breadcrumbSeparator">
                >
            </span>
    </span>
    <span id="breadcrumbs-2" dir="ltr">
            <span id="breadcrumbs-current">Find a Clinical Trial</span>
    </span>
  </nav><section id="edit-bar-container" role="edit-bar" class="container-fluid row-fluid"></section><section id="main" role="main" class="container-fluid row-fluid"><aside id="portal-column-one" role="complementary" aria-label="Portlet Column 1" class="span3">
<div class="portletWrapper" data-portlethash="706c6f6e652e6c656674636f6c756d6e0a636f6e746578740a2f6c6363632f70617469656e74636172652f636c696e6963616c2d747269616c730a6e617669676174696f6e" id="portletwrapper-706c6f6e652e6c656674636f6c756d6e0a636f6e746578740a2f6c6363632f70617469656e74636172652f636c696e6963616c2d747269616c730a6e617669676174696f6e">
<section id="vert-nav" class="portlet portletNavigationTree">
    <nav id="leftnav" class="portletItem lastItem"><nav class="portletItem lastItem" id="leftnav">
        <ul class="navTree navTreeLevel0">
            <li class="navTreeItem navTreeTopNode">
                <div>
                   <a title="" class="contenttype-folder" href="/patientcare">
                   Patient Care
                   </a>
                </div>
            </li>
<li class="navTreeItem visualNoMarker section-for-new-patients">
        <a title="" class="state-published contenttype-document" href="/patientcare/for-new-patients">
            <span>For New Patients</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker section-our-clinics">
        <a title="" class="state-published contenttype-link" href="/lccc/patientcare/">
            <span>Our Clinics</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker navTreeCurrentNode navTreeFolderish section-clinical-trials">
        <a title="" class="state-published navTreeCurrentItem navTreeCurrentNode navTreeFolderish contenttype-folder" href="/patientcare/clinical-trials">
            <span>Find a Clinical Trial</span>
        </a>
            <ul class="navTree navTreeLevel1">
<li class="navTreeItem visualNoMarker section-find-clinical-trials-by-cancer-types">
        <a title="UNC Cancer Clinical Trials - Displayed are sites for which there is an open trial." class="state-published contenttype-document" href="{$oncorehost}SIPBrowseServlet?hdn_function=DISEASE_BROWSE&amp;param_name=disease_site&amp;category=DISEASE_SITE&amp;category_description=Disease%20Site">
            <span>Find Clinical Trials by Cancer Types</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker section-find-clinical-trials-by-doctors">
        <a title="UNC Cancer Clinical Trials - Primary Investigators" class="state-published contenttype-document" href="{$oncorehost}SIPBrowseServlet?hdn_function=STAFF_BROWSE&amp;param_name=principal_investigator&amp;staff_role=PI&amp;staff_role_description=Physician">
            <span>Find Clinical Trials by Doctors</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker section-find-clinical-trials-by-drugs">
        <a title="UNC Cancer Clinical Trials - Drugs in open clinical trials" class="state-published contenttype-document" href="{$oncorehost}SIPBrowseServlet?hdn_function=DOMAIN_BROWSE&amp;param_name=drug&amp;param_value=&amp;category=DRUG&amp;category_description=Drug">
            <span>Find Clinical Trials by Drugs</span>
        </a>
</li>
<!-- <li class="navTreeItem visualNoMarker section-find-clinical-trials-by-flow-charts">
        <a title="UNC Cancer Clinical Trials - Flow Charts.  For ongoing clinical trials for cancer treatment, please call 919-966-4432 or (toll free) 1-877-668-0683." class="state-published contenttype-document" href="/patientcare/clinical-trials/find-clinical-trials-by-flow-charts">
            <span>Find Clinical Trials by Flow Charts</span>
        </a>
</li> -->
<li class="navTreeItem visualNoMarker section-find-clinical-trials-by-advanced-search">
        <a title="" class="state-published contenttype-document" href="{$oncorehost}SIPMain">
            <span>Find Clinical Trials by Advanced Search</span>
        </a>
</li>
            </ul>
</li>
<li class="navTreeItem visualNoMarker section-refer-a-patient">
        <a title="" class="state-published contenttype-link" href="/lccc/referring/">
            <span>For Referring Physicians</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker section-comprehensive-cancer-support-program">
        <a title="" class="state-published contenttype-link" href="/lccc/patientcare/programs/ccsp">
            <span>Comprehensive Cancer Support Program</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker navTreeFolderish section-services-and-support">
        <a title="" class="state-published navTreeFolderish contenttype-folder" href="/patientcare/services-and-support">

            <span>Services and Support</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker navTreeFolderish section-screening-and-prevention">
        <a title="" class="state-published navTreeFolderish contenttype-folder" href="/patientcare/screening-and-prevention">
            <span>Screening and Prevention</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker section-unc-cancer-network">
        <a title="" class="state-published contenttype-link" href="/lccc/unc-cancer-network/">
            <span>UNC Cancer Network</span>
        </a>
</li>
        </ul>
    </nav></nav>
</section>
</div>
<div class="portletWrapper" data-portlethash="706c6f6e652e6c656674636f6c756d6e0a636f6e746578740a2f6c6363632f70617469656e74636172652f636c696e6963616c2d747269616c730a6c6561726e2d61626f75742d636c696e6963616c2d747269616c732d7472656174696e672d63616e636572" id="portletwrapper-706c6f6e652e6c656674636f6c756d6e0a636f6e746578740a2f6c6363632f70617469656e74636172652f636c696e6963616c2d747269616c730a6c6561726e2d61626f75742d636c696e6963616c2d747269616c732d7472656174696e672d63616e636572">
<dl class="portlet portletStaticText portlet-static-learn-about-clinical-trials-treating-cancer">
    <dt class="portletHeader">
        <span class="portletTopLeft"></span>
        <span>
           Learn About Clinical Trials Treating Cancer
        </span>
        <span class="portletTopRight"></span>
    </dt>
    <dd class="portletItem odd">
        <p>Use the links above to search for clinical trials by cancer type, by doctor, by drug type, by flowcharts, or by keyword (advanced search). For each trial, a short description is provided along with a link to eligibility information.</p>
<p>For questions or additional information about cancer clinical trials at UNC, <b>please call 919-966-4432 or (toll free) 1-877-668-0683</b>.</p>
            <span class="portletBottomLeft"></span>
            <span class="portletBottomRight"></span>
    </dd>
</dl>
</div>
<div class="portletWrapper" data-portlethash="706c6f6e652e6c656674636f6c756d6e0a636f6e746578740a2f6c6363632f70617469656e74636172650a6765742d7468652d6c61746573742d6e657773" id="portletwrapper-706c6f6e652e6c656674636f6c756d6e0a636f6e746578740a2f6c6363632f70617469656e74636172650a6765742d7468652d6c61746573742d6e657773">
<dl class="portlet portletStaticText portlet-static-get-the-latest-news">
    <dt class="portletHeader">
        <span class="portletTopLeft"></span>
        <span>
           Get the Latest News
        </span>
        <span class="portletTopRight"></span>
    </dt>
    <dd class="portletItem odd">
        <p><a href="/patientcare/clinical-trials/subscribe/enews" class="internal-link">Subscribe to E-News</a></p>
            <span class="portletBottomLeft"></span>
            <span class="portletBottomRight"></span>
    </dd>
</dl>
</div>
        </aside>
        <div id="content">




<!-- begin sip xsl -->
<div id="noprint">
<!-- <table cellspacing="0" cellpadding="0" border="0" class="top-banner">
	<tbody><tr>
		<td width="200"><img src="/sip/images/menu/banner_left.png" alt="OnCore Logo" /></td>
		<td align="center" id="loginTitle">Study Information Portal</td>
		<td width="200" align="right" id="userTool"><a href="http://www.forteresearch.com"><img alt="Powered By Forte Research Systems" src="/sip/images/menu/banner_right.png" /></a></td>
	</tr>
</tbody></table> -->
</div>
<div style="padding:10px;">
<table border="0" cellpadding="0" cellspacing="0" ><!-- width="1014 align="left"" -->
	<tr>
		<td>
<div class="onc-table-shadow">
	<div class="table-shadow2">
<!-- 		<div class="item-header"><span class="title" id="appTitle">SIP Home</span></div> -->
		<div class="item-entries">
<table border="0" cellspacing="0" cellpadding="0" width="100%" style="border-collapse:collapse;">
<tr>
<td style="vertical-align:top;width:28%;padding:5px;">
<form method="get"
      action="{$oncorehost}SIPControlServlet"
      name="frm_sip_search"
      onsubmit="return validate();">
<!-- target="content"  action="{$oncorehost}SIPMain" -->
<input type="hidden" name="hdn_function" value="SIP_PROTOCOL_LISTINGS" />
<input type="hidden" name="disease_site_txt" value="" />
<input type="hidden" name="keyword_txt" value="" />
<input type="hidden" name="drug_txt" value="" />
<input type="hidden" name="therapy_txt" value="" />
<input type="hidden" name="phase_txt" value="" />
<input type="hidden" name="cancer_prevention_txt" value="" />
<input type="hidden" name="cancer_control_txt" value="" />
<input type="hidden" name="protocol_no_txt" value="" />
<input type="hidden" name="pi_txt" value="" />
<input type="hidden" name="study_site_txt" value="" />
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tr><xsl:apply-templates select="age_group"/></tr>
	<tr>
		<td id="ds" class="field-title" width="80" align="right">Disease Site</td>
		<td>
			<input id="autocomplete-type" placeholder="Cancer Type" type="text" name="disease_site" size="18" data-provide="typeahead" />
			<!-- <a href="javascript:doNothing()" onclick="displayDiseaseResults('DISEASE_SITE', 'Disease Site', 'disease_site')">
			<img src="{$oncorehost}images/browse.png" border="0" alt="browse" style="vertical-align: middle; padding: 0pt 2px;" /></a> -->
		</td>
	</tr>
	<tr>
		<td id="key" class="field-title" width="80" align="right">Keyword</td>
		<td><input type="text" name="keyword" size="20" /></td>
	</tr>
	<tr>
		<td id="drg" class="field-title" width="80" align="right">Drug</td>
		<td>
			<input id="autocomplete-drug" placeholder="Drug Type" type="text" name="drug" size="18" data-provide="typeahead" />
<!-- 			<a href="javascript:doNothing()" onclick="displayDomainResults('DRUG', 'Drug', 'drug')">
			<img src="{$oncorehost}images/browse.png" border="0" alt="browse" style="vertical-align: middle; padding: 0pt 2px;" /></a> -->
		</td>
	</tr>
	<xsl:apply-templates select="modality"/> <!-- Therapy -->
	<xsl:apply-templates select="modality/exclude"/>
	<xsl:apply-templates select="phase" />
	<xsl:apply-templates select="oncology_group" />
	<xsl:apply-templates select="management_group" />
<!--
	<tr><td colspan="2" class="field-title" width="260" align="right" valign="top">Phase I<input type="checkbox" name="phase_flag" value="Y" /></td></tr>
-->
	<tr>
		<td id="cp" class="field-title" width="260" align="right" valign="top"><label for="prevention_flag">Cancer Prevention</label></td>
		<td><input id="prevention_flag" type="checkbox" name="prevention_flag" value="Y" /></td>
	</tr>
	<tr>
		<td id="cc" class="field-title" width="260" align="right" valign="top"><label for="control_flag">Cancer Control</label></td>
		<td><input id="control_flag" type="checkbox" name="control_flag" value="Y" /></td>
	</tr>
	<tr>
		<td id="pcl" class="field-title" width="80" align="right">Protocol No.</td>
		<td><input type="text" name="protocol_no" size="20" /></td>
	</tr>
	<tr>
		<td id="pi" class="field-title" width="80" align="right">PI</td>
		<td>
			<input  id="autocomplete-pi" placeholder="Principal Investigator Name" type="text" name="principal_investigator" size="18" data-provide="typeahead"/>
<!-- 			<a href="javascript:doNothing()" onclick="displayStaffResults('PI','Physician', 'principal_investigator')">
		 	<img src="{$oncorehost}images/browse.png" border="0" alt="browse" style="vertical-align: middle; padding: 0pt 2px;" /></a> -->
		</td>
	</tr>
	<tr>
		<td id="ss" class="field-title" width="80" align="right">Study Site</td>
		<td>
			<input  id="autocomplete-study_site" placeholder="Site Name" type="text" name="study_site" size="18" data-provide="typeahead" />
<!-- 			<a href="javascript:doNothing()" onclick="displayDomainResults('STUDY_SITE','Study Site', 'study_site')">
			<img src="{$oncorehost}images/browse.png" border="0" alt="browse" style="vertical-align: middle; padding: 0pt 2px;" /></a> -->
		</td>
	</tr>
  <tr>
		<td colspan="2" align="right" class="chromBtn">
			<input type="submit" class="btn btn-primary" value="Search"/>
		<!-- 	<a href="#" class="btn btn-primary" style="margin:10px" onclick="searchSubmit()">Search</a> -->
			<a href="#" onclick="document.frm_sip_search.reset()" class="btn" style="margin:10px">Clear</a>
		</td>
	</tr>
	<tr><td colspan="2" align="center"><font size="6" class="field-title"></font></td></tr>
			</table>
			</form>
			</td>
			<td style="vertical-align:top;width:72%;top;padding:5px;">
				<div id="mainContent">
					<table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
						<tr>
							<td class="field-title2" align="left">
								To search for a clinical trial (protocol), use any or all of the search fields to the left:
								<ul>
								<li> Type search criteria in any of the text fields. Skip any items that are unknown or not applicable.</li>
								<li>Use the dropdown menus if you wish to limit your search by Age Group</li>
								<li>To limit the search to Phase I, Cancer Prevention or Cancer Control, check the appropriate boxes  </li>
<!-- 								<li>Browse by Disease Site, Drug, Principal Investigator (PI) or Study Site, use the magnification icon <img border="0" src="{$oncorehost}/images/browse.png" align="top" alt="browse" /> to the right of the field</li> -->
								</ul>
								<p>For all searches and for all browse results, select the Search button at the bottom after you have refined your  criteria. </p>
								<p><em><strong>Notes:</strong></em></p>
								<div>
								<p><strong>Phase I</strong> clinical trials are typically not disease specific and can be searched separately by clicking on the Phase I checkbox. (If you do not select Phase I, trials in any phase that match your other search terms will be retrieved.)</p>
								<p><strong>Cancer Prevention</strong> clinical trials test whether taking certain medicines, vitamins, minerals, or food supplements may lower the risk of a certain type of cancer.</p>
								<p><strong>Cancer Control</strong> clinical trials test whether certain drugs reduce side effects from chemotherapy and other primary treatments.</p>
								</div>
								<!-- <p>For further information on our clinical trials, please call the Demo Cancer Center cancer information line at 1-800-xxx-xxxx</p>
								<p>&#160;</p> -->
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
		</div>
		<div class="item-footer"><span style="text-align:left; float:left;">SIP Version <xsl:apply-templates select="sipVersion" /></span>
		<span><xsl:value-of select="copyright" disable-output-escaping="yes"/></span></div>
	</div>
</div>
		</td>
	</tr>
</table>
</div>
<!-- end sip xsl -->


</div></section>
        <div id="portlets-footer" aria-label="Footer Portlets" class="container-fluid">
</div><footer role="contentinfo" aria-label="Site Footer">
    <section id="localFooter" aria-label="Departmental Info &amp; Links" class="item-fluid row-fluid">
    <section class="cell SiteFooterPortletManager1 width-full position-0">
<div id="portletwrapper-436f6e74656e7457656c6c506f72746c6574732e53697465466f6f746572506f72746c65744d616e61676572310a636f6e746578740a2f6c6363630a666f6f7465722d636f6e74656e74" class="portletWrapper kssattr-portlethash-436f6e74656e7457656c6c506f72746c6574732e53697465466f6f746572506f72746c65744d616e61676572310a636f6e746578740a2f6c6363630a666f6f7465722d636f6e74656e74"><div class="portletStaticText portlet-static-footer-columns"><p>
<link href="resolveuid/6fdaffea100d40938e800afb0108e2d5" rel="stylesheet" type="text/css" /></p>
<div class="column">
<h3><span class="blue"><a href="http://www.unclineberger.org/patients-physicians/">Patients &amp; Physicians</a></span></h3>
<ul>
<li><a href="http://www.unclineberger.org/appointments">Make an Appointment</a></li>
<li><a href="http://www.unclineberger.org/patientcare/clinical-trials">Find a Clinical Trial</a></li>
<li><a href="http://www.unclineberger.org/patientcare/patient-care">Clinical Care Programs</a></li>
<li><a href="http://www.unclineberger.org/ccsp">Comprehensive Cancer Support</a></li>
<li><a href="http://findadoc.unchealthcare.org/" target="_blank">Find a Doctor</a></li>
<li><a href="http://www.unclineberger.org/patients-physicians/" title="More for patients and physicians">More ...</a></li>
</ul>
</div>
<div class="column">
<h3><span class="blue"><a href="http://www.unclineberger.org/waystohelp">Donors &amp; Friends</a></span></h3>
<ul>
<li><a href="http://www.unclineberger.org/waystohelp/giving">Giving</a></li>
<li><a href="http://www.unclineberger.org/news">News</a></li>
<li><a href="http://www.unclineberger.org/waystohelp/volunteer">Volunteer</a></li>
<li><a href="http://www.unclineberger.org/waystohelp/connect-and-share">Connect &amp; Share</a></li>
<li><a href="http://www.unclineberger.org/waystohelp/attend">Events</a></li>
<li><a href="http://www.unclineberger.org/waystohelp" title="More for donors and friends">More ....</a></li>
</ul>
</div>
<div class="column">
<h3><span class="blue"><a href="http://www.unclineberger.org/staff/">Faculty &amp; Staff</a></span></h3>
<ul>
<li><a href="http://www.unclineberger.org/research/core-facilities">Core Facilities</a></li>
<li><a href="http://www.unclineberger.org/research/research-programs">Research Programs</a></li>
<li><a href="http://www.unclineberger.org/members">Faculty Profiles</a></li>
<li><a href="http://www.unclineberger.org/research/overview">Cancer Center Membership</a></li>
<li><a href="http://www.unclineberger.org/protocolreview/protocolreview">Protocol Review Committee</a></li>
<li><a href="http://www.unclineberger.org/staff/" title="More for faculty and staff">More ...</a></li>
</ul>
</div>
<div class="column">
<h3><span class="blue"><a href="http://www.unclineberger.org/trainees/">Trainees</a></span></h3>
<ul>
<li><a href="http://www.unclineberger.org/seminars-and-symposia/">Symposia &amp; Seminars</a></li>
<li><a href="http://www.unclineberger.org/training/programs#predoctoral">Predoctoral Fellowships</a></li>
<li><a href="http://www.unclineberger.org/training/programs#postdoctoral">Postdoctoral Fellowships</a></li>
<li><a href="http://www.unclineberger.org/training/programs#clinical">Clinical Fellowships</a></li>
<li><a href="http://www.unclineberger.org/trainees/" title="More for Trainees">More ...</a></li>
</ul>
</div>
<p>&#160;</p>
<p>&#160;</p>
<div style="margin: 0 auto; text-align: center; "><img src="/landing/images/NCI_CCC-logo.png" style="margin: 0 2em 2em 0; width: 235px;" title="NCI_CCC-logo.png" height="100" width="235" alt="NCI_CCC-logo.png" class="image-inline" /> <img src="/landing/images/ucrf-logo.gif" style="width: 187px;" title="ucrf-logo.gif" height="60" width="187" alt="ucrf-logo.gif" class="image-inline" /></div></div>
</div>
</section>
    </section>
    <section id="globalFooter" aria-label="School of Medicine Info &amp; Links" class="container-fluid">
      <div class="sections row-fluid">
                <section aria-label="Logo &amp; Tagline" class="span4">
                    <a id="footerLogo" href="http://www.med.unc.edu" target="_blank" title="UNC School of Medicine" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'med.unc.edu',,true]);"><img src="/++theme++SOMTheme/img/som_logo.png" alt="UNC School of Medicine logo" /></a>
                    <p id="footerTagline">Our vision is to be the nation's leading public school of medicine.</p>
                </section>
                <div class="span2">
                    <a class="btn btn-inverse btn-navigation" data-target=".findWrapper" data-toggle="collapse">Find</a>
                    <section class="findWrapper nav-collapse" title="Find">
                        <h5>Find</h5>
                        <ul>
                            <li><a href="http://www.med.unc.edu/contact" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'med.unc.edu/contact',,true]);">Contact</a></li>
                            <li><a href="http://dir.unc.edu/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'dir.unc.edu',,true]);">UNC Directory</a></li>
                            <li><a href="http://www.med.unc.edu/maps" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'med.unc.edu/maps',,true]);">Maps &amp; Directions</a></li>
                            <li><a href="http://findadoc.unchealthcare.org/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'findadoc.unchealthcare.org',,true]);">Find a Doctor</a></li>
                            <li><a href="http://www.med.unc.edu/careers" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'med.unc.edu/careers',,true]);">Careers</a></li>
                            <li><a href="https://csg.unch.unc.edu" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'csg.unch.unc.edu',,true]);">UNC Health Care Citrix</a></li>
                        </ul>
                    </section>
                </div>
                <div class="span2">
                    <a class="btn btn-inverse btn-navigation" data-target=".aboutWrapper" data-toggle="collapse">About</a>
                    <section class="aboutWrapper nav-collapse" title="About">
                        <h5 id="about">About</h5>
                        <ul>
                            <li><a href="/sitemap" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'sitemap',,true]);">Site Map</a></li>
                            <li><a href="/accessibility-info" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'accessibility-info',,true]);">Accessibility</a></li>
                            <li><a href="http://www.med.unc.edu/privacy" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'med.unc.edu/privacy',,true]);">Privacy</a></li>
                            <li><a href="http://news.unchealthcare.org" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'news.unchealthcare.org',,true]);">News</a></li>
              <li><a href="http://www.med.unc.edu/www/events" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'med.unc.edu/www/events',,true]);">Calendar</a></li>
                            <li><a href="http://www.alert.unc.edu/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'alert.unc.edu',,true]);">Alert Carolina</a></li>
                        </ul>
                    </section>
                </div>
                <div class="span2">
                    <a class="btn btn-inverse btn-navigation" data-target=".connectWrapper" data-toggle="collapse">Connect</a>
                    <section class="connectWrapper nav-collapse" title="Connect">
                        <h5 id="connect">Connect</h5>
                        <ul>
                            <li><a href="http://www.youtube.com/uncmedicine" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'youtube',,true]);">YouTube</a></li>
                            <li><a href="https://twitter.com/unc_health_care" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'twitter',,true]);">Twitter</a></li>
                            <li><a href="https://www.facebook.com/UNCSOM" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'facebook',,true]);">Facebook</a></li>
                            <li><a href="feed://news.unchealthcare.org/landingpage_aggregator/news/aggregator/RSS" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'RSS',,true]);">News RSS</a></li>
                            <li><a href="http://help.med.unc.edu" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'help.med.unc.edu',,true]);">IT Support</a></li>
                            <li><a href="http://www.med.unc.edu/gift" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'med.unc.edu/gift',,true]);">Make a Gift</a></li>
                        </ul>
                    </section>
                </div>
                <section aria-label="Partner Sites &amp; Telephone Operators" class="span2 lastcol">
                    <h5>Partner Sites</h5>
                    <ul>
                        <li><a href="https://www.unchealthcare.org" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'unchealthcare.org',,true]);">UNC Health Care</a></li>
                        <li><a href="http://unc.edu" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'unc.edu',,true]);">UNC Chapel Hill</a></li>
                    </ul>
                    <h5>Operators</h5>
                    <ul>
                        <li>University Operator:<br /><a href="tel://1-919-962-2211" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'UNC Operator',,true]);">919-962-2211</a></li>
                        <li>UNC Hospitals' Operator:<br /><a href="tel://1-919-966-4131" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'UNCH Operator',,true]);">919-966-4131</a></li>
                    </ul>
                </section>
            </div>
            <p class="legal">© 2014 University of North Carolina at Chapel Hill School of Medicine</p>
    </section>
      <a id="top-link" data-spy="affix" data-offset-top="200" href="#top" title="Back to Top" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedBackToTop', 'backtotop',,true]);">↑ Top</a>
  </footer>



</body>
</html>
</xsl:template>


<xsl:template match="age_group">
	<td class="field-title" width="80" align="right">Age Group</td>
	<td class="field-data">
	<xsl:element name="select">
		<xsl:attribute name="name">age_group</xsl:attribute>
		<xsl:attribute name="class">selectBox</xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
	</td>
</xsl:template>

<xsl:template match="age_group/option">
		<xsl:element name="option">
			<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			<xsl:if test="@default='yes'">
			  <xsl:attribute name="selected">selected</xsl:attribute>
      </xsl:if>
			<xsl:value-of select="text"/>
		</xsl:element>
</xsl:template>

<xsl:template match="modality">
	<tr>
		<td id="ther" class="field-title" width="80" align="right">Therapy</td>
		<td class="field-data">
		<xsl:element name="select">
			<xsl:attribute name="name">therapy</xsl:attribute>
			<xsl:attribute name="class">selectBox</xsl:attribute>
			<xsl:apply-templates select="option"/>
		</xsl:element>
		</td>
	</tr>
</xsl:template>

<xsl:template match="option">
		<xsl:element name="option">
			<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			<xsl:if test="@default='yes'">
			  <xsl:attribute name="selected">selected</xsl:attribute>
      </xsl:if>
			<xsl:value-of select="text"/>
		</xsl:element>
</xsl:template>

<xsl:template match="modality/exclude">
	<tr>
		<td class="field-title" width="80" align="right" valign="top"><label for="exclude_flag">Exclude this therapy?</label></td>
		<td>
		<xsl:element name="input">
			<xsl:attribute name="type">checkbox</xsl:attribute>
			<xsl:attribute name="name">exclude_flag</xsl:attribute>
			<xsl:attribute name="id">exclude_flag</xsl:attribute>
			<xsl:attribute name="value">Y</xsl:attribute>
			<xsl:if test="@default='yes'">
					<xsl:attribute name="checked">checked</xsl:attribute>
			</xsl:if>
		</xsl:element>
		</td>
	</tr>
</xsl:template>

<xsl:template match="phase">
	<tr>
		<td id="phs" class="field-title" width="80" align="right">Phase</td>
		<td class="field-data">
		<xsl:element name="select">
			<xsl:attribute name="name">phase</xsl:attribute>
			<xsl:attribute name="class">selectBox</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
		</td>
	</tr>
</xsl:template>

<xsl:template match="phase/option">
		<xsl:element name="option">
			<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			<xsl:if test="@default='yes'">
			  <xsl:attribute name="selected">selected</xsl:attribute>
      </xsl:if>
			<xsl:value-of select="text"/>
		</xsl:element>
</xsl:template>

<xsl:template match="oncology_group">
	<tr>
		<td id="oncology_group" class="field-title" width="80" align="right"><xsl:value-of select="DOWG_name"/></td>
		<td class="field-data">
		<xsl:element name="select">
			<xsl:attribute name="name">oncology_group</xsl:attribute>
			<xsl:attribute name="class">selectBox</xsl:attribute>
			<xsl:apply-templates select="option" />
		</xsl:element>
		</td>
	</tr>
</xsl:template>

<xsl:template match="oncology_group/option">
		<xsl:element name="option">
			<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			<xsl:if test="@default='yes'">
			  <xsl:attribute name="selected">selected</xsl:attribute>
      		</xsl:if>
			<xsl:value-of select="text"/>
		</xsl:element>
</xsl:template>

<xsl:template match="management_group">
	<tr>
		<td id="management_group" class="field-title" width="80" align="right">Management Group</td>
		<td class="field-data">
		<xsl:element name="select">
			<xsl:attribute name="name">management_group</xsl:attribute>
			<xsl:attribute name="class">selectBox</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
		</td>
	</tr>
</xsl:template>

<xsl:template match="management_group/option">
		<xsl:element name="option">
			<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			<xsl:if test="@default='yes'">
			  <xsl:attribute name="selected">selected</xsl:attribute>
      </xsl:if>
			<xsl:value-of select="text"/>
		</xsl:element>
</xsl:template>

</xsl:stylesheet>
