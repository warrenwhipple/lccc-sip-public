<?xml version="1.0" encoding="iso-8859-1"?> 

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" indent="yes"/>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="protocol_summary">
    <html lang="en"> 

    <head>
      <script type="text/javascript" src="/sip/scripts/sipUtil.js"></script>
      <script type="text/javascript" src="/sip/scripts/sipStyle.js"></script>
      <script type="text/javascript">
		function displayAttachment(fileName, attachmentId)
		{
			var url = '/sip/SIPControlServlet?hdn_function=PCL_ATTACHMENTS&amp;hdn_function_type=INQUIRY';
			url = url + '&amp;attachment_id=' + attachmentId + '&amp;protocol_id=&amp;file_name=' + fileName;
			MM_openBrWindow(url,'Attachments','scrollbars=yes,resizable=yes');
		}
	  </script>

        <title>SIP Protocol Summary</title>
        <link rel="stylesheet" type="text/css" media="print" href="styles/sip_print.css" />
        <style type="text/css">
	  	  #search {display: none;}
        </style>
      </head>
      <body class="welcome-body3">
      <form method="post" action="" name="frm_sip_protocol_summary">
      <div id="noprint">
      <table border="0" cellpadding="0" cellspacing="0" width="650">
      	<tr>
      	  <td width="50%">
      	    <a href="javascript:history.back(1)">Back to Protocol Listing</a>
      	    <!--<a href="javascript:parent.content.print();">Print</a>-->
      	  </td>
      	</tr>
      </table>
      </div>
      <table border="0" cellpadding="2" cellspacing="0" align="left" class="table-color" width="650">
        <tr class="tbar-color">
          <td class="form-heading">Protocol Summary</td>
        </tr>
        <tr>
        	<td>
        		<table class="simple-table-non-rep">
        			<tbody>
				        <tr>
				          <xsl:apply-templates select="protocol/no"/>
				          <xsl:apply-templates select="protocol/PI"/>
				        </tr>
				        <tr>
				          <xsl:apply-templates select="protocol/phase"/>
				        </tr>
				        <tr>
					  			<xsl:apply-templates select="protocol/age_group"/>
					  			<xsl:apply-templates select="protocol/scope"/>
				        </tr>
				        <tr>
					  			<xsl:apply-templates select="protocol/secondary_protocol_no"/>
				        </tr>
				        <!--
				        <tr>
					  			<xsl:apply-templates select="protocol/sponsor_type"/>
				        </tr>
				        -->
				        <tr>
					  			<xsl:apply-templates select="protocol/title"/>
				        </tr>
				        <tr>
					  			<xsl:apply-templates select="protocol/objective"/>
				        </tr>
				        <tr>
					  			<xsl:apply-templates select="protocol/treatment"/>
				        </tr>
				        <tr>
					  			<xsl:apply-templates select="protocol/lay_description"/>
				        </tr>
				        <tr>
					  			<xsl:apply-templates select="protocol/eligibility"/>
				        </tr>
				        <tr>
					  			<xsl:apply-templates select="protocol/disease_site"/>
				        </tr>
				        <tr>
					  			<xsl:apply-templates select="protocol/histology"/>
				        </tr>
				        <tr>
					  			<xsl:apply-templates select="protocol/modality"/>
				        </tr>
				        <tr>
					  			<xsl:apply-templates select="protocol/drug"/>
				        </tr>
				        <tr>
					  			<xsl:apply-templates select="protocol/status"/>
				        </tr>
				        <tr>
					  			<xsl:apply-templates select="protocol/institution"/>
				        </tr>
				        <tr>
					  			<xsl:apply-templates select="protocol/detailed_eligibility"/>
				        </tr>
				        <tr>
					  			<xsl:apply-templates select="protocol/eligibility_file"/>
				        </tr>
				        <tr>
					  			<xsl:apply-templates select="protocol/other_attachments"/>
				        </tr>
				        <!--
				        <tr>
				        	<xsl:apply-templates select="protocol/treatment_type"/>
				        </tr>
				        -->
				       	<xsl:apply-templates select="protocol/item"/>
        			</tbody>
        		</table>
        	</td>
        </tr>

        <tr class="tbar-color">
        	<td>
				<table align="center" border="0" cellpadding="0" cellspacing="0">
					<tr>
					<td align="center" class="copyright">Copyright &#169; 2001-2007 PercipEnz Technologies, Inc. All rights reserved.</td>
					</tr>
				</table>
			</td>
    	</tr>
      </table>
      </form>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="id">
    
  </xsl:template>
  
  <xsl:template match="protocol/no">
      <th width="175">Protocol No.</th>
      <td width="175"><xsl:value-of select="."/></td>
  </xsl:template>

  <xsl:template match="protocol/status">
        <th width="175">Status</th>
        <td width="175"><xsl:value-of select="."/></td>
  </xsl:template>
  
  <xsl:template match="protocol/secondary_protocol_no">
      <th>Secondary Protocol No.</th>
      <td><xsl:value-of select="."/></td>
  </xsl:template>
  
  <xsl:template match="protocol/title">
    <tr>
        <th>Title</th>
        <td colspan="3"><xsl:value-of select="."/></td>
    </tr>
  </xsl:template>
  
  <xsl:template match="protocol/objective">
    <tr>
        <th>Objective</th>
        <td colspan="3"><xsl:value-of select="."/></td>
    </tr>
  </xsl:template>
  
  <xsl:template match="protocol/treatment">
    <tr>
      <th>Treatment</th>
      <td colspan="3"><xsl:value-of select="." /></td>
    </tr>
  </xsl:template>
  
  <xsl:template match="protocol/treatment_type">
    <th>Treatment Type</th>
    <td colspan="3">xsl:value-of select="."/></td>
  </xsl:template>
  
  <xsl:template match="protocol/phase">
    <th>Phase</th>
    <td colspan="3"><xsl:value-of select="."/></td>
  </xsl:template>
  
  <xsl:template match="protocol/scope">
    <th>Scope</th>
    <td><xsl:value-of select="."/></td>
  </xsl:template>
  
  <xsl:template match="protocol/age_group">
    <th>Age Group</th>
    <td><xsl:value-of select="."/></td>
  </xsl:template>
  
  <xsl:template match="protocol/sponsor_type">
    <th>Sponsor Type</th>
    <td><xsl:value-of select="."/></td>
  </xsl:template>
  
  <xsl:template match="protocol/disease_site">
    <th>Applicable Disease Sites</th>
    <td colspan="3"><xsl:call-template name="break"/></td>
  </xsl:template>
  
  <xsl:template match="protocol/modality">
    <th>Therapies Involved</th>
    <td colspan="3"><xsl:call-template name="break"/></td>
  </xsl:template>
  
  <xsl:template match="protocol/histology">
    <th>Applicable Histologies</th>
    <td colspan="3"><xsl:call-template name="break"/></td>
  </xsl:template>
  
  <xsl:template match="protocol/drug">
    <th>Drugs Involved</th>
    <td colspan="3"><xsl:call-template name="break"/></td>
  </xsl:template>
  
  <xsl:template match="protocol/eligibility">
    <th>Key Eligibility</th>
    <td colspan="3"><xsl:value-of select="." /></td>
  </xsl:template>
  
  <xsl:template match="protocol/lay_description">
    <th>Description</th>
    <td colspan="3"><xsl:value-of select="." /></td>
  </xsl:template>
  
  <xsl:template match="protocol/PI">
    <th>Principal Investigator</th>
    <td colspan="3"><xsl:value-of select="."/></td>
  </xsl:template>
  
  <xsl:template match="protocol/item">
	<tr>
    	<th>
    		<xsl:if test="position()=1">
    			<xsl:text>Contact</xsl:text>
    		</xsl:if>
    	</th>
    	<td>
    		<xsl:value-of select="item_description"/>
    	</td>
    	<td copspan="2">
      		Phone:<xsl:value-of select="phone_no"/><br />
      		Pager:<xsl:value-of select="pager_no"/><br />
			Email:<xsl:value-of select="email"/>
    	</td>
	</tr>
  </xsl:template>
  
  <xsl:template match="protocol/institution">
    <th>Participating Institutions</th>
    <td colspan="3"><xsl:call-template name="break"/></td>
  </xsl:template>
  
  <xsl:template match="protocol/eligibility_file">
    <th>Eligibility Document</th>
    <td colspan="3">
      <a>
		<xsl:attribute name="href">javascript:displayAttachment('<xsl:value-of select="."/>','<xsl:value-of select="@id"/>')</xsl:attribute>
		<xsl:value-of select="."/>
	  </a>
    </td>
  </xsl:template>
  
  <xsl:template match="protocol/other_attachments">
    <th>Other Attachments</th>
    <td colspan="3"><xsl:apply-templates select="attachment"/></td>
  </xsl:template>
  
  <xsl:template match="attachment">
 	<a>
		<xsl:attribute name="href">javascript:displayAttachment('<xsl:value-of select="."/>','<xsl:value-of select="@id"/>')</xsl:attribute>
		<xsl:value-of select="."/>
	</a>
	<br />
  </xsl:template>
  
  <xsl:template match="protocol/detailed_eligibility">
    <th>Detailed Eligibility</th>
    <td colspan="3"><xsl:value-of select="." /></td>
  </xsl:template>

  <xsl:template name="break">
   <xsl:param name="text" select="."/>
   <xsl:choose>
     <xsl:when test="contains($text, ';')">
      <xsl:value-of select="substring-before($text, ';')"/>
      <br/>
      <xsl:call-template name="break">
          <xsl:with-param name="text" select="substring-after($text,';')"/>
      </xsl:call-template>
     </xsl:when>
     <xsl:otherwise>
	  <xsl:value-of select="$text"/>
     </xsl:otherwise>
   </xsl:choose>
  </xsl:template>

</xsl:stylesheet>

