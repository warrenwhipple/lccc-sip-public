<?xml version="1.0" encoding="iso-8859-1"?>  
 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    version="1.0">
<xsl:output method="html" indent="yes" version="4.0"/>

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>
  
<xsl:template match="sip_search">
<html lang="en">

<head>
<title>SIP Search</title>

<script type="text/javascript" src="/sip/scripts/sipUtil.js"></script>
<script type="text/javascript" src="/sip/scripts/sipStyle.js"></script>
<script type="text/javascript">
	function displayDiseaseResults(category,categoryDescription,paramName,hdnParamName)
	{
			var paramValue = eval('document.frm_sip_search.'+paramName+'.value');  

			var url = '/sip/SIPBrowseServlet?hdn_function=DISEASE_BROWSE&amp;param_name='+ paramName + '&amp;param_value=' + paramValue + '&amp;hdn_param_name=' + hdnParamName + '&amp;category=' + category + '&amp;category_description=' + categoryDescription;
			MM_openBrWindow(url,'BrowseResults','scrollbars=yes,resizable=no,dependent=yes,left=450,top=180,width=500,height=400');    

			return true;
	}

	function displayDomainResults(category,categoryDescription,paramName)
	{
			var paramValue = eval('document.frm_sip_search.'+paramName+'.value');  

			var url = '/sip/SIPBrowseServlet?hdn_function=DOMAIN_BROWSE&amp;param_name='+ paramName + '&amp;param_value=' + paramValue + '&amp;category=' + category + '&amp;category_description=' + categoryDescription;
			MM_openBrWindow(url,'BrowseResults','scrollbars=yes,resizable=no,dependent=yes,left=450,top=180,width=500,height=400');    

			return true;
	}

	function displayStaffResults(staffRole,staffRoleDescription,paramName)
	{
			var pi = document.getElementById("pi").innerHTML;
			var paramValue = eval('document.frm_sip_search.'+paramName+'.value'); 

			var url = '/sip/SIPBrowseServlet?hdn_function=STAFF_BROWSE&amp;param_name='+ paramName + '&amp;param_value=' + paramValue + '&amp;staff_role=' + pi + '&amp;staff_role_description=' + staffRoleDescription;
			MM_openBrWindow(url,'BrowseResults','scrollbars=yes,resizable=yes,dependent=yes,left=450,top=180,width=500,height=400');    

			return true;
	}

	function setParamValue(paramName,paramValue)
	{
			eval('document.frm_sip_search.'+paramName+'.value="'+paramValue+'"');
	}

	function doNothing(){}

	function validate() 
	{
		var msg = "";
		if(eval(document.frm_sip_search.exclude_flag))
		{
			var excludeFlag = document.frm_sip_search.exclude_flag.checked;	
			var therapy = document.frm_sip_search.therapy.value;

			if (excludeFlag &amp;&amp; isEmpty(therapy))
					msg = "Please enter therapy to be excluded."
			if (msg.length &gt; 0) 
			{
				alert(msg);
				return false;
			}
		}
		setSearchParamTxt();
		return true;
	}
	
	function setSearchParamTxt()
	{
		document.frm_sip_search.disease_site_txt.value=document.getElementById("ds").innerHTML;
		document.frm_sip_search.keyword_txt.value=document.getElementById("key").innerHTML;
		document.frm_sip_search.drug_txt.value=document.getElementById("drg").innerHTML;
		document.frm_sip_search.therapy_txt.value=document.getElementById("ther").innerHTML;
		document.frm_sip_search.phase_txt.value=document.getElementById("phs").innerHTML;
		document.frm_sip_search.cancer_prevention_txt.value=document.getElementById("cp").innerHTML;
		document.frm_sip_search.cancer_control_txt.value=document.getElementById("cc").innerHTML;
		document.frm_sip_search.protocol_no_txt.value=document.getElementById("pcl").innerHTML;
		document.frm_sip_search.pi_txt.value=document.getElementById("pi").innerHTML;
		document.frm_sip_search.study_site_txt.value=document.getElementById("ss").innerHTML;
	}
</script>

<link rel="stylesheet" type="text/css" media="print" href="styles/sip_print.css" />

</head>
<body class="welcome-body1">
<form method="post" 
      action="/sip/SIPControlServlet?hdn_function=SIP_PROTOCOL_LISTINGS" 
      name="frm_sip_search" target="content"
      onsubmit="return validate();">

<input type="hidden" name="hdn_function" value="" />

<input type="hidden" name="disease_site_txt" value="" />
<input type="hidden" name="keyword_txt" value="" />
<input type="hidden" name="drug_txt" value="" />
<input type="hidden" name="therapy_txt" value="" />
<input type="hidden" name="phase_txt" value="" />
<input type="hidden" name="cancer_prevention_txt" value="" />
<input type="hidden" name="cancer_control_txt" value="" />
<input type="hidden" name="protocol_no_txt" value="" />
<input type="hidden" name="pi_txt" value="" />
<input type="hidden" name="study_site_txt" value="" />

<div id = "noprint">
<table border="0" cellpadding="2" cellspacing="1" align="left" class="table-color" width="260">
	<tr class="tbar-color">
		<td class="form-heading" colspan="2">Protocol Search</td>
	</tr>
	<tr>
		<xsl:apply-templates select="age_group"/>
	</tr>
	<tr>
		<td id="ds" class="field-title" width="80" align="right">Disease Site</td>
		<td>
			<input type="text" name="disease_site" size="20" maxlength="" />
			<a href="javascript:doNothing()" onclick="displayDiseaseResults('DISEASE_SITE', 'Disease Site', 'disease_site')">
			<img src="/sip/images/browse.gif" border="0"></img></a>
		</td>
	</tr>
	<tr>
		<td id="key" class="field-title" width="80" align="right">Keyword</td>
		<td>
			<input type="text" name="keyword" size="20" maxlength="" />
		</td>
	</tr>
	<tr>
		<td id="drg" class="field-title" width="80" align="right">Drug</td>
		<td>
			<input type="text" name="drug" size="20" maxlength="" />
			<a href="javascript:doNothing()" onclick="displayDomainResults('DRUG', 'Drug', 'drug')">
			<img valign="center" src="/sip/images/browse.gif" border="0"></img></a>
		</td>
	</tr>
	
	<xsl:apply-templates select="modality"/> <!-- Therapy -->
	
	<xsl:apply-templates select="phase" />
<!--
	<tr>
		<td colspan="2" class="field-title" width="260" align="right" valign="top">Phase I 
			<input type="checkbox" name="phase_flag" value="Y" />
		</td>
	</tr>
-->
	<tr>
		<td id="cp" class="field-title" width="260" align="right" valign="top">Cancer Prevention</td>
		<td>
			<input type="checkbox" name="prevention_flag" value="Y" />
		</td>
	</tr>
	<tr>
		<td id="cc" class="field-title" width="260" align="right" valign="top">Cancer Control</td>
		<td>
			<input type="checkbox" name="control_flag" value="Y" />
		</td>
	</tr>
	
	<tr>
		<td id="pcl" class="field-title" width="80" align="right">Protocol No.</td>
		<td>
			<input type="text" name="protocol_no" size="20" maxlength="" />
		</td>
	</tr>
	<tr>
		<td id="pi" class="field-title" width="80" align="right">PI</td>
		<td>
			<input type="text" name="principal_investigator" size="20" maxlength="" />
			<a href="javascript:doNothing()" onclick="displayStaffResults('PI','Physician', 'principal_investigator')">
		 	<img valign="center" src="/sip/images/browse.gif" border="0"></img></a>
		</td>
	</tr>
	<tr>
		<td id="ss" class="field-title" width="80" align="right">Study Site</td>
		<td>
			<input type="text" name="study_site" size="20" maxlength="" />
			<a href="javascript:doNothing()" onclick="displayDomainResults('STUDY_SITE','Study Site', 'study_site')">
			<img src="/sip/images/browse.gif" border="0"></img></a>
		</td>
	</tr>
  <tr>
		<td colspan="2" align="center"> 
			<input type="submit" name="Submit" value="Search" />
			<input name="clear"  type="reset" value="Clear" />
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center"><font size="6" class="field-title"></font></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<table>
				<tr>
					<td valign="center"><font size="2" face="Arial, Helvetica"><b>Powered by</b></font></td>
					<td align="left" valign="center">
						<img src="/sip/images/percip-75.jpg" border="0"></img>
					</td>
		    </tr>
		  </table>
		</td>
	</tr>
</table>
</div>
</form>
</body>
</html>
</xsl:template>

<xsl:template match="age_group">
	<td class="field-title" width="80" align="right">Age Group</td>
	<td class="field-data">
	<xsl:element name="select">
		<xsl:attribute name="name">age_group</xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
	</td>
</xsl:template>

<xsl:template match="age_group/option">
		<xsl:element name="option">
			<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			<xsl:if test="@default='yes'">
			  <xsl:attribute name="selected">selected</xsl:attribute>
      </xsl:if>
			<xsl:value-of select="text"/>
		</xsl:element>
</xsl:template>

<xsl:template match="modality">
	<tr>
		<td id="ther" class="field-title" width="80" align="right">Therapy</td>
		<td class="field-data">
		<xsl:element name="select">
			<xsl:attribute name="name">therapy</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
		</td>
	</tr>
	<xsl:apply-templates select="modality/exclude"/>
</xsl:template>

<xsl:template match="modality/option">
		<xsl:element name="option">
			<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			<xsl:if test="@default='yes'">
			  <xsl:attribute name="selected">selected</xsl:attribute>
      </xsl:if>
			<xsl:value-of select="text"/>
		</xsl:element>
</xsl:template>

<xsl:template match="modality/exclude">
	<tr>
		<td class="field-title" width="80" align="right" valign="top"/>
		<td>
		<xsl:element name="input">
			<xsl:attribute name="type">checkbox</xsl:attribute>
			<xsl:attribute name="name">exclude_flag</xsl:attribute>
			<xsl:attribute name="value">Y</xsl:attribute>
			<xsl:if test="@default='yes'">
					<xsl:attribute name="checked">checked</xsl:attribute>
			</xsl:if>
		</xsl:element>
		Exclude this therapy?
		<br />
		</td>
	</tr>
</xsl:template>

<xsl:template match="phase">
	<tr>
		<td id="phs" class="field-title" width="80" align="right">Phase</td>
		<td class="field-data">
		<xsl:element name="select">
			<xsl:attribute name="name">phase</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
		</td>
	</tr>
</xsl:template>

<xsl:template match="phase/option">
		<xsl:element name="option">
			<xsl:attribute name="value"><xsl:value-of select="value"/></xsl:attribute>
			<xsl:if test="@default='yes'">
			  <xsl:attribute name="selected">selected</xsl:attribute>
      </xsl:if>
			<xsl:value-of select="text"/>
		</xsl:element>
</xsl:template>

</xsl:stylesheet>
