<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" indent="yes"/>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="protocols">

<xsl:param name="oncorehost">https://oncore.unc.edu/sip/</xsl:param>
<xsl:param name="wwwhost">https://unclineberger.org/</xsl:param>
<html lang="en">

<head>
<title>Study Information Portal</title>

<meta content="Clinical trials are extremely important to cancer patients, who may have exhausted all other forms of treatment, yet still have active disease." name="DC.description" />
<meta content="Clinical trials are extremely important to cancer patients, who may have exhausted all other forms of treatment, yet still have active disease." name="description" />
<meta content="text/html" name="DC.format" />
<meta content="Page" name="DC.type" />
<meta content="en" name="DC.language" />
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<base href="{$wwwhost}" />
<!-- Stylesheet blob scrapped from site in development mode -->
<link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/reset.css" /><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/member.css" /><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/collective.js.jqueryui.custom.min.css" /><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/authoring.css" /><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/++resource++tinymce.stylesheets/tinymce.css" /><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/++resource++plone.app.jquerytools.dateinput.css" /><style type="text/css" media="all">@import url(/portal_css/uncsom.SOMTheme/++resource++ContentWellPortlets.styles/ContentWellPortlets.css);</style><style type="text/css" media="screen">@import url(/portal_css/uncsom.SOMTheme/++resource++easyslider-portlet.css);</style><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/++resource++plone.formwidget.datetime/styles.css" /><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/++resource++plone.app.event/event.css" /><style type="text/css" media="screen">@import url(/portal_css/uncsom.SOMTheme/++resource++plone.formwidget.autocomplete/jquery.autocomplete.css);</style><style type="text/css" media="screen">@import url(/portal_css/uncsom.SOMTheme/++resource++plone.formwidget.contenttree/contenttree.css);</style><style type="text/css" media="screen">@import url(/portal_css/uncsom.SOMTheme/carousel.css);</style><link rel="stylesheet" type="text/css" media="all" href="/portal_css/uncsom.SOMTheme/++resource++collective.js.fullcalendar/fullcalendar.css" /><style type="text/css" media="all">@import url(/portal_css/uncsom.SOMTheme/solgema_contextualcontentmenu.css);</style><style type="text/css" media="screen">@import url(/portal_css/uncsom.SOMTheme/++resource++collective.js.colorpicker.css);</style><style type="text/css" media="all">@import url(/portal_css/uncsom.SOMTheme/som_gss.css);</style><link rel="stylesheet" type="text/css" href="/portal_css/uncsom.SOMTheme/++theme++SOMTheme/css/bootstrap.min.css" /><link rel="stylesheet" type="text/css" href="/portal_css/uncsom.SOMTheme/++theme++SOMTheme/css/bootstrap-responsive.min.css" /><link rel="stylesheet" type="text/css" href="/portal_css/uncsom.SOMTheme/++theme++SOMTheme/css/bootstrap-custom.css" /><link rel="stylesheet" type="text/css" media="screen" href="/portal_css/uncsom.SOMTheme/++theme++SOMTheme/css/ploneui.css" /><link rel="stylesheet" type="text/css" href="/portal_css/uncsom.SOMTheme/++theme++SOMTheme/css/theme.css" /><link rel="stylesheet" type="text/css" media="all" href="/portal_css/uncsom.SOMTheme/++resource++plone.formwidget.recurrence/jquery.recurrenceinput.css" /><link rel="stylesheet" type="text/css" media="all" href="/portal_css/uncsom.SOMTheme/++resource++plone.formwidget.recurrence/integration.css" /><link rel="stylesheet" type="text/css" href="/portal_css/uncsom.SOMTheme/ploneCustom.css" />
<style>
#portal-header {
    background: url("/++theme++SOMTheme/img/shadow.png") repeat-x scroll left bottom, url("/lccc-patientcare-banner") no-repeat scroll left 30px lightgray;
}
</style>
<style>
.item-color1{
    background: lightgray;
}
.item-header{
	font-size:1.5em;
}
</style>

<!-- Scrape of js in development mode -->
<script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.jquery.js"></script>

<!-- <script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/jquery-integration.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.jquerytools.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.jquerytools.dateinput.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.jquerytools.overlayhelpers.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.jquerytools.form.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/register_function.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/plone_javascript_variables.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/nodeutilities.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/cookie_functions.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/livesearch.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++search.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/select_all.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/dragdropreorder.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/collapsiblesections.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/form_tabbing.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/popupforms.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/jquery.highlightsearchterms.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/first_input_focus.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/accessibility.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/styleswitcher.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/toc.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/collapsibleformfields.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.discussion.javascripts/comments.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/dropdown.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/table_sorter.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/calendar_formfield.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/formUnload.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/formsubmithelpers.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/unlockOnFormUnload.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/tiny_mce.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/jquery.tinymce.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/tiny_mce_gzip.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/tiny_mce_init.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/kss-bbb.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/inline_validation.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++easyslider-portlet.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.event/event.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.app.event.portlet_calendar.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.formwidget.autocomplete/jquery.autocomplete.min.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.formwidget.autocomplete/formwidget-autocomplete.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.formwidget.contenttree/contenttree.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/collective.js.jqueryui.custom.min.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/jquery.tools.min.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/carousel.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++dropdown-menu.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++collective.js.fullcalendar/fullcalendar.min.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++collective.js.fullcalendar/fullcalendar.gcal.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/solgema_contextualcontentmenu.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++collective.js.colorpicker.js/eye.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++collective.js.colorpicker.js/utils.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++collective.js.colorpicker.js/colorpicker.js"></script> -->

<script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++theme++SOMTheme/js/bootstrap.min.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++theme++SOMTheme/js/theme.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.formwidget.recurrence/jquery.tmpl-beta1.js"></script><script type="text/javascript" src="/portal_javascripts/uncsom.SOMTheme/++resource++plone.formwidget.recurrence/jquery.recurrenceinput.js"></script>

<link rel="canonical" href="/patientcare/clinical-trials" />
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<link rel="apple-touch-icon" href="/touch_icon.png" />



<link rel="search" href="/@@search" title="Search this site" /><meta name="generator" content="Plone - http://plone.org" />
  <title>Clinical Trials - UNC Lineberger Comprehensive Cancer Center</title><title>Clinical Trials — UNC Lineberger Comprehensive Cancer Center</title>
  <!-- Typekit web fonts --><!--[if gt IE 8]><!-->
  <script type="text/javascript" src="//use.typekit.net/kak8yao.js"></script>
  <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
  <!--<![endif]-->
  <!-- Put the following javascript before the closing </head> tag. -->
<script>
  (function() {
    var cx = '017059784719810698204:lj2ibo0i4wo';
    var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s);
  })();
</script>
</head>


<body class="welcome-body1 template-document_view portaltype-document site-lccc section-patientcare subsection-clinical-trials subsection-clinical-trials-clinical-trials userrole-anonymous" id="" dir="ltr">
        <header id="portal-header" role="banner" aria-label="Site Header">
        <section id="top" aria-label="Common Links &amp; Search" class="navbar navbar-static-top container-fluid">
            <a class="btn btn-navbar" data-target=".ribbon-group" data-toggle="collapse" aria-labelledby="top">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
      </a>
            <a class="brand" href="http://www.med.unc.edu" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'med.unc.edu',,true]);">UNC School of Medicine</a>
      <div class="nav-collapse collapse ribbon-group pull-right">
                <ul class="link-group">
                    <li><a href="http://unc.edu/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'unc.edu',,true]);">UNC Chapel Hill</a></li>
                    <li><a href="https://www.unchealthcare.org/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'unchealthcare.org',,true]);">UNC Health Care</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'popular links',,true]);">Popular Links</a>
                        <ul class="dropdown-menu">
                            <li><a href="http://dir.unc.edu/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'dir.unc.edu',,true]);">UNC Directory</a></li>
                            <li><a href="http://www.med.unc.edu/maps" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'med.unc.edu/maps',,true]);">Maps &amp; Directions</a></li>
                            <li><a href="http://hsl.lib.unc.edu/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'hsl.lib.unc.edu',,true]);">Health Sciences Library</a></li>
                            <li><a href="https://outlook.unc.edu/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'outlook.unc.edu',,true]);">Webmail</a></li>
                            <li><a href="http://help.med.unc.edu/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'help.med.unc.edu',,true]);">IT Support</a></li>
                            <li><a href="http://www.med.unc.edu/gift" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'med.unc.edu/gift',,true]);">Make a Gift</a></li>
                            <li><a href="http://findadoc.unchealthcare.org/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'findadoc.unchealthcare.org',,true]);">Find a Doctor</a></li>
                            <li><a href="http://www.med.unc.edu/www/events" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'med.unc.edu/www/events',,true]);">Calendar</a></li>
                            <li><a href="http://www.med.unc.edu/careers" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromRibbon', 'med.unc.edu/careers',,true]);">Careers</a></li>
                        </ul>
                    </li>
                </ul>
                <div class="gcse-searchbox-only" data-resultsUrl="google_search_results"></div>
                <span id="account-info">
  <li id="anon-personalbar">

        <a href="/login" id="personaltools-login">Log in</a>

  </li>
</span>
      </div>
    </section>
    <!-- H1s for both site name and page title OK within new HTML sectioning elements -->
    <section id="site_identity" aria-label="Site Identity" class="item-fluid">
        <div id="site-logo">
            <a id="site-logo-link" title="" href="">
                <img src="lccc-logo" alt="" />
            </a>
        </div>
    </section>

<nav id="horz-nav" role="navigation" aria-label="Horizontal Navigation" class="container-fluid ">
      <a class="btn btn-navigation" data-target=".nav-group" data-toggle="collapse">Navigation</a>
        <ul id="portal-globalnav" class="nav-collapse collapse nav-group navTreeLevel0">
        <li id="portaltab-about" class="plain"><a href="/about" class="plain" title="About the UNC Lineberger Comprehensive Cancer Center and the N.C. Cancer Hospital at the University of North Carolina at Chapel Hill, including map &amp; directions and contact information.">About Us</a>
        </li><li id="portaltab-patientcare" class="selected"><a href="/patientcare" class="plain" title="">Patient Care</a></li>
        <li id="portaltab-research" class="plain"><a href="/research" class="plain" title="">Research</a></li>
        <li id="portaltab-training" class="plain"><a href="/training" class="plain" title="">Training</a></li>
        <li id="portaltab-waystohelp" class="plain"><a href="/waystohelp" class="plain" title="">Ways to Help</a></li>
        <li id="portaltab-contact" class="plain"><a href="/contact" class="plain" title="">Contact</a></li>
        </ul>
</nav>

</header><div id="portlets-header" aria-label="Header Portlets" class="container-fluid">
    </div><nav id="breadcrumbs" aria-label="Breadcrumbs" class="item-fluid">
    <span id="breadcrumbs-home">
        <a class="icon-home" href="">Home</a>
        <span class="breadcrumbSeparator">
            >
        </span>
    </span>
    <span id="breadcrumbs-1" dir="ltr">

            <a href="/patientcare">Patient Care</a>
            <span class="breadcrumbSeparator">
                >
            </span>
    </span>
    <span id="breadcrumbs-2" dir="ltr">
            <span id="breadcrumbs-current">Find a Clinical Trial</span>
    </span>
  </nav><section id="edit-bar-container" role="edit-bar" class="container-fluid row-fluid"></section><section id="main" role="main" class="container-fluid row-fluid"><aside id="portal-column-one" role="complementary" aria-label="Portlet Column 1" class="span3">
<div class="portletWrapper" data-portlethash="706c6f6e652e6c656674636f6c756d6e0a636f6e746578740a2f6c6363632f70617469656e74636172652f636c696e6963616c2d747269616c730a6e617669676174696f6e" id="portletwrapper-706c6f6e652e6c656674636f6c756d6e0a636f6e746578740a2f6c6363632f70617469656e74636172652f636c696e6963616c2d747269616c730a6e617669676174696f6e">
<section id="vert-nav" class="portlet portletNavigationTree">
    <nav id="leftnav" class="portletItem lastItem"><nav class="portletItem lastItem" id="leftnav">
        <ul class="navTree navTreeLevel0">
            <li class="navTreeItem navTreeTopNode">
                <div>
                   <a title="" class="contenttype-folder" href="/patientcare">
                   Patient Care
                   </a>
                </div>
            </li>
<li class="navTreeItem visualNoMarker section-for-new-patients">
        <a title="" class="state-published contenttype-document" href="/patientcare/for-new-patients">
            <span>For New Patients</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker section-our-clinics">
        <a title="" class="state-published contenttype-link" href="/lccc/patientcare/">
            <span>Our Clinics</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker navTreeCurrentNode navTreeFolderish section-clinical-trials">
        <a title="" class="state-published navTreeCurrentItem navTreeCurrentNode navTreeFolderish contenttype-folder" href="/patientcare/clinical-trials">
            <span>Find a Clinical Trial</span>
        </a>
            <ul class="navTree navTreeLevel1">
<li class="navTreeItem visualNoMarker section-find-clinical-trials-by-cancer-types">
        <a title="UNC Cancer Clinical Trials - Displayed are sites for which there is an open trial." class="state-published contenttype-document" href="{$oncorehost}SIPBrowseServlet?hdn_function=DISEASE_BROWSE&amp;param_name=disease_site&amp;category=DISEASE_SITE&amp;category_description=Disease%20Site">
            <span>Find Clinical Trials by Cancer Types</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker section-find-clinical-trials-by-doctors">
        <a title="UNC Cancer Clinical Trials - Primary Investigators" class="state-published contenttype-document" href="{$oncorehost}SIPBrowseServlet?hdn_function=STAFF_BROWSE&amp;param_name=principal_investigator&amp;staff_role=PI&amp;staff_role_description=Physician">
            <span>Find Clinical Trials by Doctors</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker section-find-clinical-trials-by-drugs">
        <a title="UNC Cancer Clinical Trials - Drugs in open clinical trials" class="state-published contenttype-document" href="{$oncorehost}SIPBrowseServlet?hdn_function=DOMAIN_BROWSE&amp;param_name=drug&amp;param_value=&amp;category=DRUG&amp;category_description=Drug">
            <span>Find Clinical Trials by Drugs</span>
        </a>
</li>
<!-- <li class="navTreeItem visualNoMarker section-find-clinical-trials-by-flow-charts">
        <a title="UNC Cancer Clinical Trials - Flow Charts.  For ongoing clinical trials for cancer treatment, please call 919-966-4432 or (toll free) 1-877-668-0683." class="state-published contenttype-document" href="/patientcare/clinical-trials/find-clinical-trials-by-flow-charts">
            <span>Find Clinical Trials by Flow Charts</span>
        </a>
</li> -->
<li class="navTreeItem visualNoMarker section-find-clinical-trials-by-advanced-search">
        <a title="" class="state-published contenttype-document" href="{$oncorehost}SIPMain">
            <span>Find Clinical Trials by Advanced Search</span>
        </a>
</li>
            </ul>
</li>
<li class="navTreeItem visualNoMarker section-refer-a-patient">
        <a title="" class="state-published contenttype-link" href="/lccc/referring/">
            <span>For Referring Physicians</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker section-comprehensive-cancer-support-program">
        <a title="" class="state-published contenttype-link" href="/lccc/patientcare/programs/ccsp">
            <span>Comprehensive Cancer Support Program</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker navTreeFolderish section-services-and-support">
        <a title="" class="state-published navTreeFolderish contenttype-folder" href="/patientcare/services-and-support">

            <span>Services and Support</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker navTreeFolderish section-screening-and-prevention">
        <a title="" class="state-published navTreeFolderish contenttype-folder" href="/patientcare/screening-and-prevention">
            <span>Screening and Prevention</span>
        </a>
</li>
<li class="navTreeItem visualNoMarker section-unc-cancer-network">
        <a title="" class="state-published contenttype-link" href="/lccc/unc-cancer-network/">
            <span>UNC Cancer Network</span>
        </a>
</li>
        </ul>
    </nav></nav>
</section>
</div>
<div class="portletWrapper" data-portlethash="706c6f6e652e6c656674636f6c756d6e0a636f6e746578740a2f6c6363632f70617469656e74636172652f636c696e6963616c2d747269616c730a6c6561726e2d61626f75742d636c696e6963616c2d747269616c732d7472656174696e672d63616e636572" id="portletwrapper-706c6f6e652e6c656674636f6c756d6e0a636f6e746578740a2f6c6363632f70617469656e74636172652f636c696e6963616c2d747269616c730a6c6561726e2d61626f75742d636c696e6963616c2d747269616c732d7472656174696e672d63616e636572">
<dl class="portlet portletStaticText portlet-static-learn-about-clinical-trials-treating-cancer">
    <dt class="portletHeader">
        <span class="portletTopLeft"></span>
        <span>
           Learn About Clinical Trials Treating Cancer
        </span>
        <span class="portletTopRight"></span>
    </dt>
    <dd class="portletItem odd">
        <p>Use the links above to search for clinical trials by cancer type, by doctor, by drug type, by flowcharts, or by keyword (advanced search). For each trial, a short description is provided along with a link to eligibility information.</p>
<p>For questions or additional information about cancer clinical trials at UNC, <b>please call 919-966-4432 or (toll free) 1-877-668-0683</b>.</p>
            <span class="portletBottomLeft"></span>
            <span class="portletBottomRight"></span>
    </dd>
</dl>
</div>
<div class="portletWrapper" data-portlethash="706c6f6e652e6c656674636f6c756d6e0a636f6e746578740a2f6c6363632f70617469656e74636172650a6765742d7468652d6c61746573742d6e657773" id="portletwrapper-706c6f6e652e6c656674636f6c756d6e0a636f6e746578740a2f6c6363632f70617469656e74636172650a6765742d7468652d6c61746573742d6e657773">
<dl class="portlet portletStaticText portlet-static-get-the-latest-news">
    <dt class="portletHeader">
        <span class="portletTopLeft"></span>
        <span>
           Get the Latest News
        </span>
        <span class="portletTopRight"></span>
    </dt>
    <dd class="portletItem odd">
        <p><a href="/patientcare/clinical-trials/subscribe/enews" class="internal-link">Subscribe to E-News</a></p>
            <span class="portletBottomLeft"></span>
            <span class="portletBottomRight"></span>
    </dd>
</dl>
</div>
        </aside>
        <div id="content">



<!-- begin sip xsl -->
   		<table class="simple-table">
   			<thead>
   			<tr><th class="item-header">Protocols</th><th colspan="2"><!-- <a href="{$oncorehost}/SIPMain">Back To Search Instructions</a> --></th></tr>
	        <tr>
				<th colspan="3"><xsl:value-of select="count"/> protocol(s) meet the specified criteria</th>
	        </tr>
	        <!-- begin search parameters -->
	        <xsl:for-each select="search_params/search_param">
	        <tr>
	        <xsl:if test="position() mod 1 = 0">
				<th colspan="3">
					<font class="field-title"><xsl:value-of select="param_name"/></font>: <xsl:value-of select="param_value"/>
				</th>
			</xsl:if>
    		</tr>
	        </xsl:for-each>
	        <!-- end search parameters -->
	        <tr>
	        	<td width="15%">Protocol No.</td>
	        	<td width="75%">Title</td>
	        	<td width="10%">Status</td>
	    	</tr>
	    	</thead>
	    	<tbody>
        <xsl:if test="count=0">
			<tr><td class="center" colspan="3">No protocols found.</td></tr>
		</xsl:if>
        <xsl:if test="count &gt; 0">
	        <xsl:for-each select="protocol">
		        <tr>
					<xsl:if test="position() mod 2 = 0">
	       				<xsl:attribute name="class">item-color1</xsl:attribute>
	    			</xsl:if>

					<td class="field-data">
						<xsl:apply-templates select="no"/>
					</td>

		        	<td class="field-data">
						<xsl:apply-templates select="title"/>
					</td>

		        	<td class="field-data">
						<xsl:apply-templates select="status"/>
					</td>
		        </tr>
			</xsl:for-each>
		</xsl:if>
			</tbody>
		</table>
<!-- end sip xsl -->


</div></section>
        <div id="portlets-footer" aria-label="Footer Portlets" class="container-fluid">
</div><footer role="contentinfo" aria-label="Site Footer">
    <section id="localFooter" aria-label="Departmental Info &amp; Links" class="item-fluid row-fluid">
    <section class="cell SiteFooterPortletManager1 width-full position-0">
<div id="portletwrapper-436f6e74656e7457656c6c506f72746c6574732e53697465466f6f746572506f72746c65744d616e61676572310a636f6e746578740a2f6c6363630a666f6f7465722d636f6e74656e74" class="portletWrapper kssattr-portlethash-436f6e74656e7457656c6c506f72746c6574732e53697465466f6f746572506f72746c65744d616e61676572310a636f6e746578740a2f6c6363630a666f6f7465722d636f6e74656e74"><div class="portletStaticText portlet-static-footer-columns"><p>
<link href="resolveuid/6fdaffea100d40938e800afb0108e2d5" rel="stylesheet" type="text/css" /></p>
<div class="column">
<h3><span class="blue"><a href="http://www.unclineberger.org/patients-physicians/">Patients &amp; Physicians</a></span></h3>
<ul>
<li><a href="http://www.unclineberger.org/appointments">Make an Appointment</a></li>
<li><a href="http://www.unclineberger.org/patientcare/clinical-trials">Find a Clinical Trial</a></li>
<li><a href="http://www.unclineberger.org/patientcare/patient-care">Clinical Care Programs</a></li>
<li><a href="http://www.unclineberger.org/ccsp">Comprehensive Cancer Support</a></li>
<li><a href="http://findadoc.unchealthcare.org/" target="_blank">Find a Doctor</a></li>
<li><a href="http://www.unclineberger.org/patients-physicians/" title="More for patients and physicians">More ...</a></li>
</ul>
</div>
<div class="column">
<h3><span class="blue"><a href="http://www.unclineberger.org/waystohelp">Donors &amp; Friends</a></span></h3>
<ul>
<li><a href="http://www.unclineberger.org/waystohelp/giving">Giving</a></li>
<li><a href="http://www.unclineberger.org/news">News</a></li>
<li><a href="http://www.unclineberger.org/waystohelp/volunteer">Volunteer</a></li>
<li><a href="http://www.unclineberger.org/waystohelp/connect-and-share">Connect &amp; Share</a></li>
<li><a href="http://www.unclineberger.org/waystohelp/attend">Events</a></li>
<li><a href="http://www.unclineberger.org/waystohelp" title="More for donors and friends">More ...</a></li>
</ul>
</div>
<div class="column">
<h3><span class="blue"><a href="http://www.unclineberger.org/staff/">Faculty &amp; Staff</a></span></h3>
<ul>
<li><a href="http://www.unclineberger.org/research/core-facilities">Core Facilities</a></li>
<li><a href="http://www.unclineberger.org/research/research-programs">Research Programs</a></li>
<li><a href="http://www.unclineberger.org/members">Faculty Profiles</a></li>
<li><a href="http://www.unclineberger.org/research/overview">Cancer Center Membership</a></li>
<li><a href="http://www.unclineberger.org/protocolreview/protocolreview">Protocol Review Committee</a></li>
<li><a href="http://www.unclineberger.org/staff/" title="More for faculty and staff">More ...</a></li>
</ul>
</div>
<div class="column">
<h3><span class="blue"><a href="http://www.unclineberger.org/trainees/">Trainees</a></span></h3>
<ul>
<li><a href="http://www.unclineberger.org/seminars-and-symposia/">Symposia &amp; Seminars</a></li>
<li><a href="http://www.unclineberger.org/training/programs#predoctoral">Predoctoral Fellowships</a></li>
<li><a href="http://www.unclineberger.org/training/programs#postdoctoral">Postdoctoral Fellowships</a></li>
<li><a href="http://www.unclineberger.org/training/programs#clinical">Clinical Fellowships</a></li>
<li><a href="http://www.unclineberger.org/trainees/" title="More for Trainees">More ...</a></li>
</ul>
</div>
<p>&#160;</p>
<p>&#160;</p>
<div style="margin: 0 auto; text-align: center; "><img src="/landing/images/NCI_CCC-logo.png" style="margin: 0 2em 2em 0; width: 235px;" title="NCI_CCC-logo.png" height="100" width="235" alt="NCI_CCC-logo.png" class="image-inline" /> <img src="/landing/images/ucrf-logo.gif" style="width: 187px;" title="ucrf-logo.gif" height="60" width="187" alt="ucrf-logo.gif" class="image-inline" /></div></div>
</div>
</section>
    </section>
    <section id="globalFooter" aria-label="School of Medicine Info &amp; Links" class="container-fluid">
      <div class="sections row-fluid">
                <section aria-label="Logo &amp; Tagline" class="span4">
                    <a id="footerLogo" href="http://www.med.unc.edu" target="_blank" title="UNC School of Medicine" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'med.unc.edu',,true]);"><img src="/++theme++SOMTheme/img/som_logo.png" alt="UNC School of Medicine logo" /></a>
                    <p id="footerTagline">Our vision is to be the nation's leading public school of medicine.</p>
                </section>
                <div class="span2">
                    <a class="btn btn-inverse btn-navigation" data-target=".findWrapper" data-toggle="collapse">Find</a>
                    <section class="findWrapper nav-collapse" title="Find">
                        <h5>Find</h5>
                        <ul>
                            <li><a href="http://www.med.unc.edu/contact" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'med.unc.edu/contact',,true]);">Contact</a></li>
                            <li><a href="http://dir.unc.edu/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'dir.unc.edu',,true]);">UNC Directory</a></li>
                            <li><a href="http://www.med.unc.edu/maps" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'med.unc.edu/maps',,true]);">Maps &amp; Directions</a></li>
                            <li><a href="http://findadoc.unchealthcare.org/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'findadoc.unchealthcare.org',,true]);">Find a Doctor</a></li>
                            <li><a href="http://www.med.unc.edu/careers" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'med.unc.edu/careers',,true]);">Careers</a></li>
                            <li><a href="https://csg.unch.unc.edu" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'csg.unch.unc.edu',,true]);">UNC Health Care Citrix</a></li>
                        </ul>
                    </section>
                </div>
                <div class="span2">
                    <a class="btn btn-inverse btn-navigation" data-target=".aboutWrapper" data-toggle="collapse">About</a>
                    <section class="aboutWrapper nav-collapse" title="About">
                        <h5 id="about">About</h5>
                        <ul>
                            <li><a href="/sitemap" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'sitemap',,true]);">Site Map</a></li>
                            <li><a href="/accessibility-info" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'accessibility-info',,true]);">Accessibility</a></li>
                            <li><a href="http://www.med.unc.edu/privacy" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'med.unc.edu/privacy',,true]);">Privacy</a></li>
                            <li><a href="http://news.unchealthcare.org" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'news.unchealthcare.org',,true]);">News</a></li>
              <li><a href="http://www.med.unc.edu/www/events" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'med.unc.edu/www/events',,true]);">Calendar</a></li>
                            <li><a href="http://www.alert.unc.edu/" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'alert.unc.edu',,true]);">Alert Carolina</a></li>
                        </ul>
                    </section>
                </div>
                <div class="span2">
                    <a class="btn btn-inverse btn-navigation" data-target=".connectWrapper" data-toggle="collapse">Connect</a>
                    <section class="connectWrapper nav-collapse" title="Connect">
                        <h5 id="connect">Connect</h5>
                        <ul>
                            <li><a href="http://www.youtube.com/uncmedicine" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'youtube',,true]);">YouTube</a></li>
                            <li><a href="https://twitter.com/unc_health_care" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'twitter',,true]);">Twitter</a></li>
                            <li><a href="https://www.facebook.com/UNCSOM" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'facebook',,true]);">Facebook</a></li>
                            <li><a href="feed://news.unchealthcare.org/landingpage_aggregator/news/aggregator/RSS" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'RSS',,true]);">News RSS</a></li>
                            <li><a href="http://help.med.unc.edu" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'help.med.unc.edu',,true]);">IT Support</a></li>
                            <li><a href="http://www.med.unc.edu/gift" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'med.unc.edu/gift',,true]);">Make a Gift</a></li>
                        </ul>
                    </section>
                </div>
                <section aria-label="Partner Sites &amp; Telephone Operators" class="span2 lastcol">
                    <h5>Partner Sites</h5>
                    <ul>
                        <li><a href="https://www.unchealthcare.org" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'unchealthcare.org',,true]);">UNC Health Care</a></li>
                        <li><a href="http://unc.edu" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'unc.edu',,true]);">UNC Chapel Hill</a></li>
                    </ul>
                    <h5>Operators</h5>
                    <ul>
                        <li>University Operator:<br /><a href="tel://1-919-962-2211" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'UNC Operator',,true]);">919-962-2211</a></li>
                        <li>UNC Hospitals' Operator:<br /><a href="tel://1-919-966-4131" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedFromFooter', 'UNCH Operator',,true]);">919-966-4131</a></li>
                    </ul>
                </section>
            </div>
            <p class="legal">© 2014 University of North Carolina at Chapel Hill School of Medicine</p>
    </section>
      <a id="top-link" data-spy="affix" data-offset-top="200" href="#top" title="Back to Top" onclick="_gaq.push(['_trackEvent', 'SOMTheme Links', 'ClickedBackToTop', 'backtotop',,true]);">↑ Top</a>
  </footer>



</body>
</html>

  </xsl:template>

  <xsl:template match="id">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="no">
  	<xsl:param name="oncorehost">https://oncore.unc.edu/sip/</xsl:param>
	<a>
	<xsl:attribute name="href"><xsl:value-of select="$oncorehost" />SIPControlServlet?hdn_function=SIP_PROTOCOL_SUMMARY&amp;protocol_no=<xsl:value-of select="."/>
<!--		<xsl:attribute name="href">javascript:displayProtocolSummary('<xsl:value-of select="@id"/>','<xsl:value-of select="."/>') -->
	</xsl:attribute>
		<xsl:value-of select="."/>
	</a>
  </xsl:template>

  <xsl:template match="count">

  </xsl:template>

  <xsl:template match="status">
	<xsl:value-of select="."/>
  </xsl:template>

</xsl:stylesheet>
