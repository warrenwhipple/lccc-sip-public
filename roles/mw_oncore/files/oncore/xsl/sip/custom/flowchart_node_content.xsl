<?xml version="1.0" encoding="iso-8859-1"?> 

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="yes"/>


<xsl:param name="root_id" />
<xsl:param name="node_id" />
<xsl:param name="is_overview" />

<xsl:template match="chart">

	<head>
		<script src="/sip/scripts/sipStyle.js" type="text/javascript"></script>
		
		<style>
			.buttonStyle {  
			height: 25px; 
			width: 100px
			}
		</style>
		
		<script type="text/javascript">
			
			function MM_openBrWindow(theURL,winName,features) { //v2.0
			window.open(theURL,winName,features);
			}
			
			function getDetail(myForm,protocolNo, protocolId) {
			myForm.action = "/sip/SIPControlServlet";
			myForm.hdn_function.value = "SIP_PROTOCOL_SUMMARY_FC";
			myForm.protocol_id.value = protocolId;
			myForm.protocol_no.value = protocolNo;
			myForm.submit();
			}
			
			function printPage(myForm) {
			myForm.action = "/sip/FlowchartExternalControlServlet";
			myForm.hdn_function.value = "PRINT_CHART";
			myForm.root_id.value = '<xsl:value-of select="$root_id"/>';
			myForm.node_id.value = '<xsl:value-of select="$node_id"/>';
			myForm.target = '_blank';
			myForm.submit();
			}
			
			function overview(titleIs) {
			var rootIs = "<xsl:value-of select="$root_id"/>";
			var nodeIs = "<xsl:value-of select="$node_id"/>";
			
			<![CDATA[
			var url = "/sip/FlowchartExternalControlServlet?hdn_function=OVERVIEW" +  "& root_id=" + rootIs + "&node_id="+ nodeIs + "&title=" + titleIs;
				MM_openBrWindow(url,'Flowchart','menubar=yes,scrollbars=yes,resizable=yes,dependent=yes,left=100,top=100,width=1044,height=768');
			]]>
			
			}
		</script>
	</head>

	<body>
		<form name="frm_get_detail" action="" method="post">
			<input type="hidden" name="hdn_function" value="" />
			<input type="hidden" name="protocol_id" value="" />
			<input type="hidden" name="protocol_no" value="" />
			<input type="hidden" name="root_id" value="" />
			<input type="hidden" name="node_id" value="" />
			<input type="hidden" name="title" value="" />
		</form>
	</body>
	

		<xsl:for-each select="tree_head/tree_node">
		
			<xsl:if test="position() = 1"> 
				<table border="0" cellspacing="0" cellpadding="4" width="100%">
					<tr class="tbar-buff"> 
						<th class="field-title" align="left" colspan="3"><xsl:value-of select="title"/></th> 
						<xsl:if test="$is_overview = 'Y'">
							<th class="field-title" align="right" colspan="2">
								<a>	<xsl:attribute name="href">javascript:overview('<xsl:value-of select="title"/>')</xsl:attribute>Overview</a>
							</th>
						</xsl:if>
	
					</tr> 
				</table>
				<xsl:if test="pcl_size != 0">
					<table class="simple-table-non-rep">
						<thead>
							<tr class="item-color2">
								<td rowspan="2" width="5%">Priority</td>
								<td rowspan="2" width="15%">Protocol No</td>
								<td width="50%">Correlates</td>
								<td width="50%">Status</td>
							</tr>
							<tr class="item-color2">
								<td class="field-title" colspan="2">Key Eligibility</td>
							</tr>
						</thead>
						<tbody>
						
							<xsl:for-each select="protocol">
					
								<xsl:if test="position() mod 2 = 1"> 
									<tr class="item-color1">
										<td rowspan="2"><xsl:value-of select="priority"/> </td>
										<xsl:if test="exclude = 'N'">
											<td rowspan="2"><a><xsl:attribute name="href">javascript:getDetail(document.frm_get_detail, '<xsl:value-of select="protocol_no"/>', '<xsl:value-of select="protocol_id"/>')</xsl:attribute>
												<xsl:value-of select="protocol_no"/> </a> </td>
										</xsl:if>
										<xsl:if test="exclude = 'Y'">
											<td rowspan="2"><xsl:value-of select="protocol_no"/></td>
										</xsl:if>
										<td> <xsl:value-of select="correlates_id"/> </td>
										<td> <xsl:value-of select="stat"/> </td>
									</tr>
									<tr class="item-color1">
										<td colspan="2"> <xsl:value-of select="eligibility"/> </td>
									</tr>
								</xsl:if>
								
								<xsl:if test="position() mod 2 = 0">
									<tr class="item-color2">  
										<td rowspan="2"> <xsl:value-of select="priority"/> </td>
										<xsl:if test="exclude = 'N'">
											<td rowspan="2"><a><xsl:attribute name="href">javascript:getDetail(document.frm_get_detail, '<xsl:value-of select="protocol_no"/>', '<xsl:value-of select="protocol_id"/>')</xsl:attribute>
												<xsl:value-of select="protocol_no"/> </a> </td>
										</xsl:if>
										<xsl:if test="exclude = 'Y'">
											<td rowspan="2"><xsl:value-of select="protocol_no"/></td>
										</xsl:if>									<td> <xsl:value-of select="correlates_id"/> </td>
										<td> <xsl:value-of select="stat"/> </td>
									</tr>
									<tr class="item-color2">
											<td colspan="2"> <xsl:value-of select="eligibility"/> </td>
									</tr>
								</xsl:if>
							</xsl:for-each>
							
						</tbody>
					</table>
				</xsl:if>
				<xsl:if test="pcl_size = 0">
					<table class="simple-table-non-rep">
						<tr class="item-color2"><td>No Protocols found</td></tr>
					</table>
				</xsl:if>

 			</xsl:if>
			
			<!-- Child Node -->
			<xsl:if test="position() != 1">
			<table border="0" cellspacing="0" align="center" cellpadding="4" width="90%">
					<tr class="tbar-buff"> 
						<th class="field-title" colspan="2"><xsl:value-of select="title"/></th> 
					</tr> 
			</table>
			<xsl:if test="pcl_size != 0">
				<table class="simple-table-non-rep" align="center" style="width:90%;">
					<thead>
						<tr class="item-color2">
							<td rowspan="2" width="3%">Priority</td>
							<td rowspan="2" width="12%">Protocol No</td>
							<td width="15%">Location</td>
							<td width="15%">Correlates</td>
							<td width="15%">Status</td>
						</tr>
						<tr class="item-color2">
							<td colspan="3">Key Eligibility</td>
						</tr>
					</thead>
					<tbody>
						<xsl:for-each select="protocol">	
							<xsl:if test="position() mod 2 = 1"> 
								<tr class="item-color1">
									<td rowspan="2"><xsl:value-of select="priority"/> </td>
									<xsl:if test="exclude = 'N'">
										<td rowspan="2"><a><xsl:attribute name="href">javascript:getDetail(document.frm_get_detail, '<xsl:value-of select="protocol_no"/>', '<xsl:value-of select="protocol_id"/>')</xsl:attribute>
											<xsl:value-of select="protocol_no"/> </a> </td>
									</xsl:if>
									<xsl:if test="exclude = 'Y'">
										<td rowspan="2"><xsl:value-of select="protocol_no"/></td>
									</xsl:if>									<td> <xsl:value-of select="location"/> </td>
									<td> <xsl:value-of select="correlates_id"/> </td>
									<td> <xsl:value-of select="stat"/> </td>
								</tr>
								<tr class="item-color1">
									<td colspan="3"> <xsl:value-of select="eligibility"/> </td>
								</tr>
							</xsl:if>
							
							<xsl:if test="position() mod 2 = 0">
								<tr class="item-color2">  
									<td rowspan="2"><xsl:value-of select="priority"/> </td>
									<xsl:if test="exclude = 'N'">
										<td rowspan="2"><a><xsl:attribute name="href">javascript:getDetail(document.frm_get_detail, '<xsl:value-of select="protocol_no"/>', '<xsl:value-of select="protocol_id"/>')</xsl:attribute>
											<xsl:value-of select="protocol_no"/> </a> </td>
									</xsl:if>
									<xsl:if test="exclude = 'Y'">
										<td rowspan="2"><xsl:value-of select="protocol_no"/></td>
									</xsl:if>									<td> <xsl:value-of select="location"/> </td>
									<td> <xsl:value-of select="correlates_id"/> </td>
									<td> <xsl:value-of select="stat"/> </td>
								</tr>
								<tr class="item-color2">
									<td colspan="3"> <xsl:value-of select="eligibility"/> </td>
								</tr>
							</xsl:if>
						</xsl:for-each>
					</tbody>
				</table>
				</xsl:if>
				<xsl:if test="pcl_size = 0">
					<table class="simple-table-non-rep" align="center" style="width:90%;">
						<tr class="item-color2"><td>No Protocols found</td></tr>
					</table>
				</xsl:if>
			</xsl:if>

	     </xsl:for-each>
	     <table>
		 	<tr>
				<td colspan="5" align="right">
				<button type="button" onclick="javascript:printPage(document.frm_get_detail);">Print</button>		
				</td>
			</tr>
		</table>
  </xsl:template>	
</xsl:stylesheet>
                       
